""" Issue handling functionality. Issues can originate from different platforms (Gitlab, Jira, ...).
Each origin has its own implementation based on the generic concept of an Issue. """
import re
from typing import List, Dict, Optional

from gitlab.exceptions import GitlabGetError
from requests.exceptions import HTTPError
from sahlab.gitlab import Gitlab


class TransitionNotAllowed(Exception):
    """ Exception used when a transition in Jira status is not allowed. """


class Issue():
    """ Issue base class. An issue should have a unique identifier, a title and a description.
    This class is considered abstract and should not be used directly. Based on the origin of the
    issue, the corresponding subclass should be instantiated instead. If the origin isn't known,
    then the factory method issue_create can be called instead. If the identifier is also unknown
    and only a single string of information is available, then you can also attempt to extract the
    issue information out of the string by using the alternative factory method issue_extract.
    """
    def __init__(self, identifier: str, title: str = "", description: str = ""):
        self.identifier = identifier
        self.title = title
        self.description = description

    def __eq__(self, other):
        return self.get_id() == other.get_id()

    def __lt__(self, other):
        return self.get_id() < other.get_id()

    def __hash__(self):
        return hash(self.identifier)

    def get_id(self) -> str:
        """ Returns the Issue identifier """
        return self.identifier

    def get_title(self) -> str:
        """ Returns the Issue title.
        If the title is not present, compatible subclasses might be able to fetch it from the
        Issue's origin, else an empty string is returned. """
        return self.title


    def get_description(self) -> str:
        """ Returns the Issue description.
        If the description is not present, compatible subclasses might be able to fetch it from the
        Issue's origin, else a NotImplementedError is raised. """
        if self.description:
            return self.description
        raise NotImplementedError("Lazy loading is only supported from compatible subclasses")



class JiraIssue(Issue):
    """ Jira based Issue. """
    regex: str = r"^[A-Z,0-9]{2,10}-[0-9]+$"
    """ Regex string to match the Jira Issue identifier (e.g. EX-1) """
    full_regex: str = r"^[i|I]ssue\s?:?\s+\[?\(?([A-Z,0-9]{2,10}-[0-9]+)\]?\)?:?(.*)"
    """ Regex string to match a full Jira Issue string (e.g. Issue: EX-1 This is an Issue) """

    def __init__(self, identifier: str, title: str = "",
                 description: str = "", plain_issue: bool = False):
        self.jira = None if plain_issue else Jira()
        self.issue = None
        if self.jira:
            try:
                self.issue = self.jira.jira.issue(identifier)
            except HTTPError:
                pass
        super().__init__(identifier, title, description)

    @staticmethod
    def validate(msg: str) -> bool:
        """ Validates the given string with the Jira formatting applied within Soft At Home. """
        return bool(re.match(JiraIssue.full_regex, msg))

    @staticmethod
    def dummy() -> 'JiraIssue':
        """ Return a dummy Jira issue. """
        return JiraIssue("DUMMY-0000", "- No associated bugs found", plain_issue=True)

    def __str__(self) -> str:
        return f"Issue: {self.get_id()} {self.get_title()}"



# Pylint warns about the fact that the GitlabIssue is not overriding some of the default methods.
# However that is what we want in this case, since some of the issue fields are jira specific.
# pylint: disable=abstract-method
class GitlabIssue(Issue):
    """ Gitlab based Issue. """
    regex: str = r"^[^ ]*#[0-9]+$"
    """ Regex string to match the Gitlab Issue identifier (e.g. path/to/repo#1) """
    full_regex: str = r"^[i|I]ssue\s?:?\s+\[?\(?([^ ]*)(#[0-9]+)\]?\)?:?(.*)"
    """ Regex string to match a Gitlab Issue (e.g. Issue: path/to/repo#1 This is an Issue) """

    def __init__(self, identifier: str, title: str = "",
                 description: str = "", plain_issue: bool = False):
        self.path, identifier = identifier.split('#')
        self.issue = None
        if self.path and not plain_issue:
            gitlab = Gitlab()
            try:
                self.issue = gitlab.gitlab.projects.get(self.path).issues.get(identifier)
            except GitlabGetError:
                self.path = ""
        super().__init__(identifier, title, description)

    def get_title(self) -> str:
        if not self.title and self.issue:
            self.title = self.issue.title
        return self.title

    def get_description(self) -> str:
        if not self.description and self.issue:
            self.description = self.issue.description
        return self.description

    def __str__(self) -> str:
        return f"Issue: {self.path}#{self.get_id()} {self.get_title()}"


# class BugzillaIssue(Issue):
#     """ Bugzilla based Issue (or Bug).
#     Legacy system, currently not supported and no support is expected in the near future, but
#     this is where it would go if we do. """

def issue_create(identifier: str, title: str = "", description: str = "",
                 plain_issue: bool = False) -> Issue:
    """ Factory method for Issue instances.

    This method provides a general construction mechanism for issue objects.
    It takes the same arguments as the constructors of the Issue subclasses, but infers the type
    based on the input. Generally, each type of Issue is clearly identifiable by the identifier.

    This method can be used when processing a list of issues of non-uniform origins.

    Args:
        identifier (str): The unique identifier of the Issue, used in the type inference step.
        title (str, optional): The title of the Issue, if None then it can still be fetched lazily
            (if the corresponding origin has a supporting API).
        description (str, optional): The description of the Issue, if None then it can still be
            fetched lazily (if the corresponding origin has a supporting API).
        plain_issue (bool, optional): If True, no connection with gitlab/jira is made
            in the background. Useful when no operations on the issues need to be done.
            False by default.

    Returns:
        Issue: New Issue instance, using the inferred subclass.

    Raises:
        ValueError: When the type inference cannot deduce the issue type.
    """
    if re.match(JiraIssue.regex, identifier):
        return JiraIssue(identifier, title, description, plain_issue)
    if re.match(GitlabIssue.regex, identifier):
        return GitlabIssue(identifier, title, description, plain_issue)

    # Out of ideas - raise error
    raise ValueError("Cannot deduce issue origin!")


def issue_extract(msg: str, default_dummy: bool = False, origin: str = "",
                  plain_issue: bool = False) -> List[Issue]:
    """ Factory method for Issue instances when only a vague string description is known.
    This string will likely be a commit message or the body of a MR.
    The message is parsed line-by-line and references to Issues are looked for.
    If a match is found, the corresponding issue type is created.
    Only one match per line is possible, so Issues should be seperated by line seperators.

    Args:
        msg (str): The string message to extract the issue data from
        default_dummy (bool, optional): Return default dummy issue (`JiraIssue.dummy`) if no
            information can be extracted, else raise a ValueError. Defaults to False.
        origin (str, optional): Path (with namespace) to repository the issue is being extracted
            from. Useful for definining Gitlab Issues.
        plain_issue (bool, optional): If True, no connection with gitlab/jira is made
            in the background. Useful when no operations on the issues need to be done.
            False by default.

    Returns:
        List[Issue]: New Issue instances with the extracted information, if no issue is found and
            default_dummy is set, then a dummy Jira issue is returned.

    Raises:
        ValueError: When no issue can be extracted and default_dummy is set to False.
    """
    issues: List[Issue] = []
    for line in msg.split("\n"):
        match = re.match(JiraIssue.full_regex, line)
        if match:
            issues.append(JiraIssue(match.group(1), match.group(2).strip(),
                                    plain_issue=plain_issue))
        match = re.match(GitlabIssue.full_regex, line)
        if match:
            origin = match.group(1) or origin
            issues.append(GitlabIssue(origin + match.group(2), match.group(3).strip(),
                                      plain_issue=plain_issue))

    if issues:
        return issues

    # Out of ideas - return dummy or raise error
    if default_dummy:
        return [JiraIssue.dummy()]
    raise ValueError(f"Cannot extract issue information from '{msg}'")
