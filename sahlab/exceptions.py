""" SAHLab Custom Exceptions functionality """

from gitlab.exceptions import GitlabGetError


class RefGetError(GitlabGetError):
    """
    Exception for when a certain reference was not found.

    Attributes:
        project (SAHRepo): SAHRepo project
        reference (str): reference (tag or branch) that could not be found
    """

    def __init__(self, project, reference):
        self.project = project
        self.reference = reference
        self.message = (
            f"Reference '{self.reference}', for project '{self.project}' "
            "not found."
        )
        super().__init__(self.message)


class FileGetError(GitlabGetError):
    """
    Exception raised when a certain file could not be found on Gitlab,
    usually because the given reference, project, or file does not exist.

    Attributes:
        project (SAHRepo): SAHRepo project
        reference (str): reference (tag or branch) that could not be found
        path (str): path to a given file
    """

    def __init__(self, project, reference, path):
        self.project = project
        self.reference = reference
        self.path = path
        self.message = (
            f"Failed getting file at path {self.path}, for reference "
            f"{self.reference}, in project {self.project}"
        )
        super().__init__(self.message)
