""" Changelog parsing and handling functionality. """
from typing import Dict, List, Optional
import re
from packaging import version


def list_categories() -> List[str]:
    """ Lists all supported changelog categories in their canonical order. """
    return ["break", "remove", "new", "fix", "change", "depr", "sec", "other"]


def select_category(string: str) -> Optional[str]:
    """ Checks if the given string corresponds to a known and supported category.
    Returns the category if it is supported, else returns None.
    """
    try:
        return [d for d in list_categories() if "[" + d + "]" in string][0]
    except IndexError:
        # String does not belong to an accepted category
        return None


def categorize(strings: List[str], discard: bool = True) -> Dict[str, List[str]]:
    """ Categorizes a given list of strings according to their category. If discard is set to True
    (by default) and the category cannot be determined, then the string will be left out. If
    discard is set to False and the category cannot be determined, then the string will end up
    in the category "other".
    """
    categorized_str: Dict[str, List[str]] = {}
    for string in strings:
        category = select_category(string)
        if not category:
            if discard:
                continue
            category = "other"
        if category not in categorized_str:
            categorized_str[category] = []
        categorized_str[category].append(re.sub(rf'\s*\[{category}\]', "", string))
    return {cat: categorized_str[cat] for cat in list_categories() if cat in categorized_str}


def get_changelog_content_between(start_version: str, stop_version: str,
                                  changelog: str, exclude: list = None,
                                  prefix: str = "") -> Dict[str, List[str]]:
    """ Reads out the changelog of a repo between start_version and stop_version. Returns a dict
    which collects all items per category. The input 'changelog_dict' is returned if the given repo
    does not have a changelog.
    Args:
        start_version (str): Defines the start boundary (e.g. v0.0.1)
        stop_version (str): Defines the stop boundary (e.g. v1.2.3)
        changelog (str): The changelog to read out.
        exclude (str)(Optional): Exclude specific entries.
        prefix (str)(Optional): If set, the added items to the output dict
            will be prefixed with this string.
    Returns:
        Dict[str, List[str]]: Every category is an entry in the dictionary. Each entry contains
            a list of corresponding items belonging to the category. e.g.
            {'change': ['change 1', 'change 2'], 'fix': ['fix1', 'fix2']}
    """
    changelog_dict: Dict[str, list] = {}
    exclude = exclude if exclude else []
    versions = (version.parse(stop_version), version.parse(start_version))

    # This assumes at least one release
    blocks = re.split(r'## Release ', changelog, flags=re.MULTILINE)[1:]
    for block in blocks:
        current_version = version.parse(block.split(" ")[0])
        if current_version > versions[0]:
            # Too new
            continue
        if current_version <= versions[1]:
            # Done
            break
        elements = re.split(r'### ', block, flags=re.MULTILINE)[1:]
        for element in elements:
            split = element.split("\n")
            item = split.pop(0).rstrip(" ")
            for i in filter(None, split):
                if i.lstrip("- ") in exclude:
                    continue
                if item not in changelog_dict:
                    changelog_dict[item] = []
                changelog_dict[item].append(prefix + i.lstrip("- "))
    return changelog_dict
