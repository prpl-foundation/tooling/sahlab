""" SAHLab - SAH Domain Aware Gitlab Interaction Framework """
import logging
from itertools import repeat
from multiprocessing import Pool
from typing import List

from sahlab.haystack import Haystack, flatten
from sahlab.naming import Name
from sahlab.repository import repo_create, SAHRepo, Dependency


def list_projects(project_filter: str = '.',
                  class_hint=None,
                  haystack: Haystack = None,
                  haystack_entries: list = None,
                  unique: bool = False) -> List[SAHRepo]:
    """ Returns a list of projects based on the projects defined in the `sahlab.haystack.Haystack`.

    Args:
        project_filter (str, optional): 'jq' style filter that will be applied on the haystack.
            Defaults to "'.'" to match all. For more information check `sahlab.haystack.Haystack`.
            It is important that the result can still be processed by the
            `sahlab.repository.repo_create` method, the `sahlab.haystack.flatten` call will ensure
            this, but as a user, you should make sure that your given filter is compatible!
        class_hint (class, optional): If all listed projects are of the same repository class,
            then this hint can improve performance as the repository type inference step can be
            skipped. Defaults to None. For more information check `sahlab.repository.repo_create`.
        haystack (Haystack, optional): If seeded, this given haystack will be used instead of
            fetching a new one. It will be flattened regardless. Defaults to None.
        haystack_entries (list, optional): If seeded, these entries are flattened and used to create
            the projects instead of querying the haystack.
        unique (bool, optional): If True, no duplicate project (based on project id) is returned.
            This could sometimes happen when dealing with CompositeRepo types, as for each subtype
            of the composite, a separate object could be returned (depends on the haystack query).
            Defaults to False.

    Returns:
        List[SAHRepo]: `sahlab.repository.SAHRepo` objects matching the given filter.
    """
    if not haystack:
        haystack = Haystack()
    if haystack_entries:
        haystack_array = flatten(haystack_entries)
    else:
        haystack_array = flatten(haystack.apply_filter(project_filter))
    if unique:
        haystack_array = list({e['id']: e for e in haystack_array}.values())
    return [repo_create(project_def, class_hint, haystack) for project_def in haystack_array]


def _parallel_find_component(project, component) -> List[Dependency]:
    """ Pickleable function for processing the lookup in parallel. """
    logging.debug("Looking for %s in %s/%s",
                  component, project.definition['namespace'], project.definition['name'])
    deps = []
    try:
        deps = project.find_dependency(component)
    except NotImplementedError as err:
        logging.error(str(err))
    logging.debug("Completed search for %s in %s/%s",
                  component, project.definition['namespace'], project.definition['name'])
    return deps


def find_component(component: Name, projects: List[SAHRepo]) -> List[Dependency]:
    """ Returns a list of dependencies for every matched component in the given list of search
    targets.

    Args:
        component (Name): The SAHLab Name of the component to search for
        projects (List[SAHRepo]): The list of repositories to search in (can be easily constructed
            by the `sahlab.list_projects` function).

    Returns:
        List[Dependency]: All dependencies that match the given component.
    """
    # TODO: refactor to parallel module
    # pylint: disable=consider-using-with
    # With cannot be used because we then cannot pool.close() before pool.join().
    pool = Pool(processes=16)
    try:
        results = pool.starmap(_parallel_find_component, zip(projects, repeat(component)))
    finally:
        pool.close()
        pool.join()
    dependencies: List[Dependency] = []
    list(map(dependencies.extend, filter(None, results)))
    return dependencies
