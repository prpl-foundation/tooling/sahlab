""" Utility functions """
import datetime
import os
import time


def get_unix_epoch_time(timestamp):
    """ Converts a gitlab timestamp string (e.g. 2021-05-06T16:39:45.000+02:00)
    to the number of seconds since the epoch of 1970.
    """
    return time.mktime(datetime.datetime.fromisoformat(timestamp).utctimetuple())


def master_or_main():
    """ Returns whether we're using "master" or "main" as standard branch.
    Default is "master", setting the environment variable 'USE_MAIN' switches this behavior """
    return "master" if not os.environ.get("USE_MAIN") else "main"


def determine_gitlab_api_key(url):
    """ Determine which gitlab api key to use based on a gitlab url
    e.g. https://gitlab.com/prpl-foundation/...
    """
    instance = url.replace("https://", "").split("/")[0]
    if instance == "gitlabinternal.softathome.com":
        return os.environ.get("GITLAB_API_KEY")
    if instance == "gitlab.com":
        return os.environ.get("GITLAB_API_KEY_EXTERNAL")
    return os.environ.get(f"GITLAB_API_KEY_{instance.replace('.', '_')}")
