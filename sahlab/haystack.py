""" Haystack related classes and functions. The Haystack is a data structure stored on Gitlab
containing references to the projects that take part of automated processes
like automated integration.
"""
import logging
import os
from typing import Union, List

import pyjq  # type: ignore
import yaml

from sahlab.gitlab import Gitlab


class Haystack():
    """ The Haystack is a datastructure containing easily filtered/indexed references
        to the projects on gitlab participating in automated processes. Extra enriching metadata
        accompanies the project definitions (e.g. component owner/approvers).

        The Haystack is by default looked for in the following repository:
        https://gitlab.com/soft.at.home/sofa/integration/haystack. Which version of the
        haystack to pick is determined by the environment variable 'HAYSTACK_BRANCH',
        which defaults to "master". """
    def __init__(self, branch: str = None, repo: int = 6729):
        gitlab = Gitlab()

        if not branch:
            branch = str(os.environ.get("HAYSTACK_BRANCH", "master"))
        project = gitlab.gitlab.projects.get(repo)
        logging.debug("Retrieving Haystack (haystack.yml) from branch %s", branch)
        self.__haystack = yaml.safe_load(project.files.raw("haystack.yml", ref=branch))

    def __getitem__(self, key: str):
        return self.__haystack[key]

    def to_dict(self) -> dict:
        """ Returns the contents of the Haystack object as a plain dict. """
        return self.__haystack

    def apply_filter(self, filter_query: str = '.') -> list:
        """ Applies a 'jq' style filter to the haystack dictionary.

        The JSON query tool 'jq' allows processing and manipulating JSON strings.
        The dictionary object from the haystack is compatible with JSON (as it itself is a yml).
        This filter will allow apps to process only the projects that they are interested in,
        instead of having to loop over the entire haystack, or worse the entire gitlab tree.

        For more information on how to formulate a query, refer to the official jq manual:
        https://stedolan.github.io/jq/manual/#Basicfilters.

        Args:
            filter_query (string, optional): This filter will be applied as a query. All matching
                results will be returned. Defaults to ".", which will return everything.

                Example to filter just configs: '.config'.
                Example to filter configs and metas: '.config,.meta'.

        Returns:
            list: Filtered haystack contents.\

            The returned value is no longer of the Haystack type, but is just the contents as
            requested by the filter. It will always be a list, but it could be a list of dicts,
            a list of lists or even a list of integers or strings.
        """
        return pyjq.all(filter_query, self.__haystack)


def flatten(haystack: Union[list, dict, Haystack]) -> List[dict]:
    """ Flattens a, possibly filtered, haystack dictionary to a single
        list of "project" defining dictionaries (i.e. containing an 'id' field).

        Note, this utility function is primarily usefull if we want to accept all sorts of
        filters for the haystack. It is perfectly possible to net a flattened list by formulating
        your filter query as such. For example, the query '.[] | .[]' will also result in a fully
        flattened haystack.

        Args:
            haystack (Union[list, dict, Haystack]): The Haystack to flatten. It can be a Haystack
                object, inner dict of an Haystack or a list of Haystack entries.

        Returns:
            List[dict]: Returns a list of Haystack entries. These entries project defining
                dictionaries. Each dictionary defines at least the project's identifier, but should
                also define a name and namespace. """
    if not haystack:
        # Base case, empty object
        return []
    if isinstance(haystack, Haystack):
        # Transform to inner dict object for flattening
        haystack = haystack.to_dict()
    if isinstance(haystack, dict):
        if "id" in haystack:
            # Base case, single project definition
            return [haystack]
        # Else: Grouping of project definitions, reduce to flat list of values
        return flatten(list(haystack.values()))
    if isinstance(haystack, list):
        flattened = []
        list(map(lambda h: flattened.extend(flatten(h)), haystack))
        return flattened
    # Default - invalid input
    logging.warning("Don't know how to flatten input: '%s', did you filter correctly?", haystack)
    return []
