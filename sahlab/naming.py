""" SAH Naming tends to be all over the place. This module aims to provide conversion from one
naming scheme to another. It also provides a unique way of addressing components within sahlab and
its applications.
"""
from collections import namedtuple
import copy
import csv
from datetime import datetime
import itertools
import logging
import os
from pathlib import Path
import re
from typing import Dict, List, Optional

from sahlab.gitlab import Gitlab, _get_default_instance
from sahlab.singleton import Singleton
from sahlab.utilities import master_or_main


Name = namedtuple('Name', 'path sah_name package_name yocto_name openwrt_name')
Name.__doc__ = """The Name object itself.
This is an immutable tuple describing the known names for all the different supported schemes. """
Name.path.__doc__ = """Gitlab path where you can find the component. This field is mandatory. """
Name.sah_name.__doc__ = """The SAH Name of the component.
This name is used by the SAH buildsystem and can be found in the Component.lst file. """
Name.package_name.__doc__ = """The name used by SAH Artifactory CI,
it is the gitlab repository name with hypens replaced by underscores. """
Name.yocto_name.__doc__ = """The Yocto recipe name of the component and metalayers. """
Name.openwrt_name.__doc__ = """The OpenWRT package of the component. """


def _get_raw_project(repo: int):
    gitlab = Gitlab()
    return gitlab.gitlab.projects.get(repo)


class NameBuilder:
    """ Builder class for constructing Name objects dynamically. """
    def __init__(self, path: str = None, template: Name = None):
        self.path = None
        self.sah_name = None
        self.package_name = None
        self.yocto_name = None
        self.openwrt_name = None
        if template:
            self.path = template.path
            self.sah_name = template.sah_name
            self.package_name = template.package_name
            self.yocto_name = template.yocto_name
            self.openwrt_name = template.openwrt_name
        self.path = self.path or path

    def with_path(self, path: str) -> 'NameBuilder':
        """ Update the git path of the Name under construction. """
        self.path = path
        return self

    def with_sah_name(self, sah_name: str) -> 'NameBuilder':
        """ Update the sah name of the Name under construction. """
        self.sah_name = sah_name
        return self

    def with_package_name(self, package_name: str) -> 'NameBuilder':
        """ Update the package name of the Name under construction. """
        self.package_name = package_name
        return self

    def with_yocto_name(self, yocto_name: str) -> 'NameBuilder':
        """ Update the yocto name of the Name under construction. """
        self.yocto_name = yocto_name
        return self

    def with_openwrt_name(self, openwrt_name: str) -> 'NameBuilder':
        """ Update the openwrt name of the Name under construction. """
        self.openwrt_name = openwrt_name
        return self

    def build(self) -> Name:
        """ Finalizes the Name object with the current set name values. """
        if not self.path:
            raise ValueError("Name object should at least contain reference to its git path!")
        return Name(path=self.path,
                    sah_name=self.sah_name,
                    package_name=self.package_name,
                    yocto_name=self.yocto_name,
                    openwrt_name=self.openwrt_name,
                    )


class NameFactory(metaclass=Singleton):
    """ Singleton factory class for defining name mappings.
    A mapping is always provided from the point of view of the gitlab path, as this is considered
    to always exist and unique.

    Runtime components can also be mapped, it is however possible for a single repository/gitlab
    path to house multiple runtime packages. Hence, this mapping is from the point of view of the
    Yocto (official opensource) naming.

    By default information comes from Component.lst project. Alternatives could be component mapping
    from the corresponding Yocto layer. """
    __mapping: Dict[str, Dict[str, Name]] = {}
    __runtime_mapping: Dict[str, Dict[str, Name]] = {}

    def __init__(self, provide_default_mapping: bool = True):
        if provide_default_mapping:
            self.provide_standard_mappings()

    def _extend_mapping_dict(self, git: str = ""):
        if not self.__mapping.get(git):
            self.__mapping[git] = {}
            self.__runtime_mapping[git] = {}

    def get_name_for_repo(self, path: str, git: str = "") -> Name:
        """ Retrieve a Name object from the constructed mapping for a given Gitlab path.

        Raises:
            AttributeError: If the repository path is not known. """
        git = git if git else _get_default_instance()[8:]
        if path not in self.__mapping.get(git, {}):
            raise AttributeError(f"Component naming for path '{path}' not found!")
        return self.__mapping[git][path]

    # pylint: disable=too-many-arguments
    def get_name_for_component(self, component: str, fuzzy_search: bool = False,
                               include_sah_name: bool = True,
                               include_yocto_name: bool = True,
                               include_openwrt_name: bool = True,
                               include_package_name: bool = False,
                               git: str = "") -> list:
        """ Look for a component in the mapping with a given sah_name, package_name,
        yocto_name or openwrt_name. The match should be exact unless fuzzy_search is set to True.

        Args:
            component (str): The component name to look for.
            fuzzy_search (bool, optional): The component name can be a substring instead of an
                exact match.
            include_sah_name (bool, optional): Include sah names for the search
            include_yocto_name (bool, optional): Include yocto names for the search
            include_openwrt_name (bool, optional): Include openwrt names for the search
            include_package_name (bool, optional): Include package names for the search

        Returns:
            List[Name]: List of matching Name tuples. The list is empty if no entry was found.
        """
        git = git if git else _get_default_instance()[8:]
        if not self.__mapping.get(git, None):
            return []
        if fuzzy_search:
            return list(filter(lambda e: e.sah_name and include_sah_name and
                               component in e.sah_name or
                               e.yocto_name and include_yocto_name and
                               component in e.yocto_name or
                               e.openwrt_name and include_openwrt_name and
                               component in e.openwrt_name or
                               e.package_name and include_package_name and
                               component in e.package_name,
                               self.__mapping[git].values()))

        return list(filter(lambda e: component in (e.sah_name if include_sah_name else "",
                                                   e.yocto_name if include_yocto_name else "",
                                                   e.openwrt_name if include_openwrt_name else "",
                                                   e.package_name if include_package_name else ""),
                           self.__mapping[git].values()))

    def get_runtime_name_for_repo(self, path: str,
                                  git: str = "") -> List[Name]:
        """ Look for all runtime packages that correspond to the given Gitlab path.
        Note that only exceptions are mapped, so your path might not result in an output.
        In that case, the name of the component will match the name of the runtime package.
        """
        git = git if git else _get_default_instance()[8:]
        if not self.__runtime_mapping.get(git, None):
            return []
        return list(filter(lambda e: path == e.path, self.__runtime_mapping[git].values()))

    def get_runtime_name_for_package(self, package: str,
                                     git: str = "") -> Optional[Name]:
        """ Retrieves the name object for the given package (Yocto name).
        Only exceptions are mapped, meaning that if your package is not found, then it either
        doesn't exist or has the same name accross all supported buildsystems.

        Only mapping starting from the official Yocto name is supported.

        Args:
            package (str): The Yocto name of the runtime package to look for.

        Returns:
            Name: The mapped runtime name or None if not found.
        """
        git = git if git else _get_default_instance()[8:]
        return self.__runtime_mapping.get(git, {}).get(package)


    def _apply_opensource_mapping(self, entry: dict, git: str) -> None:
        self._extend_mapping_dict(git)
        name_builder = NameBuilder(entry["path"],
                                   template=self.__mapping.get(git, {}).get(entry["path"]))
        self.__mapping[git][entry["path"]] = name_builder.with_yocto_name(entry["yocto"])\
                                                         .with_openwrt_name(entry["openwrt"])\
                                                         .build()

    def _apply_gitlab_mapping(self, entry: dict, git: str) -> None:
        self._extend_mapping_dict(git)
        name_builder = NameBuilder(entry["path"],
                                   template=self.__mapping.get(git, {}).get(entry["path"]))
        self.__mapping[git][entry["path"]] = name_builder.with_yocto_name(entry["name"])\
                                                         .with_openwrt_name(entry["name"])\
                                                         .build()

    def provide_opensource_mappings(self, branch: str = "master",
                                    repo: int = 8514,
                                    git: str = "") -> 'NameFactory':
        """ Parses the Opensource.csv file from the component-mapping buildsystems support repo.
        This file contains all opensource component mappings which are missing from the Yocto
        meta-componentlst. Openwrt mappings for opensource components can also be found in this file
        as well. This mapping should always be run in conjunction with the yocto mappings to get the
        full picture.

        Args:
            branch (str, optional): Get the file for this given branch ref, if it is not set, master
                is used by default.
            repo (int, optional): The repository id to look for the Opensource.csv file.
                https://gitlab.com/soft.at.home/sofa/buildsystems-support/component-mapping
                is used by default.

        Returns:
            NameFactory: returns itself so you can easily chain this command.
        """
        git = git if git else _get_default_instance()[8:]
        proj = _get_raw_project(repo)
        opensourcelst = proj.files.raw("Opensource.csv", ref=branch).decode("utf-8").splitlines()
        list(map(lambda c: self._apply_opensource_mapping(c, git=git),
                 csv.DictReader(opensourcelst, dialect="unix")))
        return self

    def _apply_runtime_mapping(self, entry: dict, git: str) -> None:
        self._extend_mapping_dict(git)
        name_builder = NameBuilder(entry["path"],
                                   template=self.__runtime_mapping.get(git, {}).get(entry["yocto"]))
        name_builder = name_builder.with_yocto_name(entry["yocto"])\
                                   .with_openwrt_name(entry["openwrt"])

        self.__runtime_mapping[git][entry["yocto"]] = name_builder.build()

    def provide_runtime_mappings(self, branch: str = "master", repo: int = 8514,
                                 git: str = "") -> 'NameFactory':
        """ Parses the Runtime.csv file from the component-mapping buildsystems support repo.
        This file contains all exceptions for runtime packages that don't match between buildsystems
        example of this would be the openssl runtime dependency, which is openssl for the Yocto
        buildsystem, yet libopenssl for the OpenWRT buildsystem.

        All these exceptions are mapped in the NameFactory, note that if your runtime component
        isn't found, than it means that the same name can be used for both Yocto as well as the
        OpenWRT buildsystem.

        The Yocto name is chosen to be the key and only translations starting from the Yocto name
        is supported.

        Args:
            branch (str, optional): Get the file for this given branch ref, if it is not set, master
                is used by default.
            repo (int, optional): The repository id to look for the Runtime.csv file.
                https://gitlab.com/soft.at.home/sofa/buildsystems-support/component-mapping
                is used by default.

        Returns:
            NameFactory: returns itself so you can easily chain this command.
        """
        git = git if git else _get_default_instance()[8:]
        proj = _get_raw_project(repo)
        runtimelst = proj.files.raw("Runtime.csv", ref=branch).decode("utf-8").splitlines()
        list(map(lambda c: self._apply_runtime_mapping(c, git),
                 csv.DictReader(runtimelst, dialect="unix")))
        return self


    def provide_gitlab_mappings(self, branch: str = "master",
                                repo: int = 8514,
                                file: str = "Gitlab.csv",
                                git: str = "") -> 'NameFactory':
        """ Parses a given .csv file from a component-mapping repo.
        This file contains all component mappings for components hosted on the given git instance.
        The given names refer to Yocto and Openwrt names.

        Args:
            branch (str, optional): Get the file for this given branch ref, if it is not set, master
                is used by default.
            repo (int, optional): The repository id to look for the given file.
            file (str): The file to parse
            git (str): The git instance where the mappings apply for. By default the currently
                connected git instance is used.

        Returns:
            NameFactory: returns itself so you can easily chain this command.
        """
        git = git if git else _get_default_instance()[8:]
        proj = _get_raw_project(repo)
        gitlablst = proj.files.raw(file, ref=branch).decode("utf-8").splitlines()
        list(map(lambda c: self._apply_gitlab_mapping(c, git=git),
                 csv.DictReader(gitlablst, dialect="unix")))
        return self



class SAHTag():
    """ Tags are formatted in a very specific way within Soft At Home.
    A Tag consists of its base (branch like master or proj_X) and the version (vA.B.C) which is
    versioned using semantic versioning.

    This class will assist in validating, constructing and breaking down potential tag names.
    Do not confuse objects of this class with actual git "tags", this class only handles naming.

    Raises:
        ValueError: When attempting to instantiate a SAHTag instance with an invalid tag name
    """

    # pylint: disable=too-many-instance-attributes
    # Eight is reasonable in this case.

    def __init__(self, tag, padded=False):
        self.style = ""
        self.base = None
        self.date = None
        self.letter = ""
        self.major = 0
        self.minor = 0
        self.patch = 0
        self.alpha = 0
        self.padded = padded
        if not any(map(lambda f: f(tag), [self._match_branch_style, self._match_p4_style,
                                          self._match_opensource_style])):
            raise ValueError(f"Invalid SAH Tag: {tag}")

    def _match_opensource_style(self, tag: str) -> bool:
        tag = Path(tag).name
        match = re.match(r"([av])([0-9]+)\.([0-9]+)\.([0-9]+)(\.[0-9]+)?", tag)
        if not match:
            return False
        self.style = "opensource"
        self.letter = match.group(1)
        self.major = int(match.group(2))
        self.minor = int(match.group(3))
        self.patch = int(match.group(4))
        try:
            self.alpha = int(match.group(5)[1:])
        except TypeError:
            self.alpha = None
        return True

    def _match_branch_style(self, tag: str) -> bool:
        # Sanitize refs/tags prefix
        tag = Path(tag).name
        match = re.match(r"(.+?)(\_[0-9]{4}\-[0-9]{2}\-[0-9]{2})?_"
                         r"([av])([0-9]+)\.([0-9]+)\.([0-9]+)(\.[0-9]+)?$", tag)
        if not match:
            return False
        self.style = "git"
        self.base = match.group(1)
        self.date = match.group(2).lstrip("_") if match.group(2) else None
        self.letter = match.group(3)
        self.major = int(match.group(4))
        self.minor = int(match.group(5))
        self.patch = int(match.group(6))
        try:
            self.alpha = int(match.group(7)[1:])
        except TypeError:
            self.alpha = None
        return True

    def _match_p4_style(self, tag: str) -> bool:
        match = re.match(r"(.*)_V([0-9]*)\.([0-9]*)\.([0-9]*)", tag)
        if not match:
            return False
        self.style = "p4"
        base = match.group(1).lower().replace("/", "_")
        base = re.sub(r"_?(rel|main)_[0-9]*-[0-9]*-[0-9]*",
                      "" if any(map(base.__contains__, ("proj", "gen"))) else master_or_main(),
                      base)
        self.base = base
        self.letter = "v"
        self.major = int(match.group(2))
        self.minor = int(match.group(3))
        self.patch = int(match.group(4))
        self.alpha = None
        return True

    def __str__(self) -> str:
        # pylint: disable=consider-using-f-string
        # In this case using .format() is cleaner than using a f-string
        if not self.padded:
            return "{base}{date}{letter}{major}.{minor}.{patch}{alpha}"\
                    .format(base=f"{self.base}_" if self.base else "",
                            date=f"{self.date}_" if self.date else "",
                            letter=self.letter,
                            major=self.major, minor=self.minor, patch=self.patch,
                            alpha=f".{self.alpha}" if self.alpha is not None else "")
        return "{base}{date}{letter}{major}.{minor}.{patch}{alpha}"\
               .format(base=f"{self.base}_" if self.base else "",
                       date=f"{self.date}_" if self.date else "",
                       letter=self.letter,
                       major=str(self.major).zfill(2),
                       minor=str(self.minor).zfill(2),
                       patch=str(self.patch).zfill(2),
                       alpha=f".{str(self.alpha).zfill(2)}" if self.alpha is not None else "")

    def get_version_number(self) -> str:
        """ Returns a string of the SAHTag, minus the base (branch) and letter """
        # pylint: disable=consider-using-f-string
        # In this case using .format() is cleaner than using a f-string
        if not self.padded:
            return "{major}.{minor}.{patch}{alpha}"\
                    .format(major=self.major, minor=self.minor, patch=self.patch,
                            alpha=f".{self.alpha}" if self.alpha is not None else "")
        return "{major}.{minor}.{patch}{alpha}"\
               .format(major=str(self.major).zfill(2),
                       minor=str(self.minor).zfill(2),
                       patch=str(self.patch).zfill(2),
                       alpha=f".{str(self.alpha).zfill(2)}" if self.alpha is not None else "")

    def get_branch(self) -> str:
        """ Returns the branch. Defaults to master (or main) for opensource tags.
        """
        return self.base if self.base else master_or_main()

    def increment(self, level: str = "patch") -> 'SAHTag':
        """ Increment this tag by the given level. A new tag object will be returned.

        Args:
            level (str): The level to increment the tag with, can be "major", "minor", "patch" or
                         "alpha".

        Returns:
            SAHTag: returns a copy of this tag with the given level incremented.

        Raises:
            ValueError: if the level given is not either major, minor or patch
        """
        if level not in ("major", "minor", "patch", "alpha"):
            raise ValueError(f"Unknown level '{level}' passed, can only be major, minor, patch or "
                             "alpha!")
        incremented = copy.copy(self)
        date = f"{datetime.today().strftime('%Y-%m-%d')}"
        incremented.date = None if not incremented.date else date
        setattr(incremented, level, getattr(incremented, level) + 1)
        if incremented.alpha is not None and (level == "patch"):
            incremented.alpha = 0
        elif level == "minor":
            incremented.patch = 0
            incremented.alpha = 0 if self.alpha is not None else None
        elif level == "major":
            incremented.minor = 0
            incremented.patch = 0
            incremented.alpha = 0 if self.alpha is not None else None
        return incremented

    @staticmethod
    def validate(tag: str) -> bool:
        """ Validates if the given string is a valid SAHTag.

        Args:
            tag (str): String representation of a potential SAHTag

        Returns:
            bool: Returns True if the given string can be parsed to a SAHTag object,
                  False otherwise.
        """
        try:
            SAHTag(tag)
            return True
        except (ValueError, AssertionError):
            return False

    @staticmethod
    def nil(base: str = "", letter: str = "v") -> 'SAHTag':
        """ Returns nil object of a SAHTag (i.e. base_letter0.0.0).
        Useful for comparisons after doing some Tag maths.

        Args:
            base (str, optional): The base of the tag, defaults to "master" or "main".
            letter (str, optional): The letter of the tag ("v" or "a"), defaults to "v".

        Returns:
            SAHTag: returns a SAHTag with only zeroes as its fields.
        """
        if not base:
            base = master_or_main()
        return SAHTag(f"{base}_{letter}0.0.0")

    def normalize(self) -> 'SAHTag':
        """ Returns a version of this tag which has only a 1 in its highest slot
        (major, minor, patch) and zeroes everywhere else. The original SAHTag object remains
        unchanged. As an example, the normalized SAHTag "v0.5.18" would be "v0.1.0".
        """
        normal = copy.copy(self)
        flag = 1
        normal.major = min(self.major, flag)
        flag = flag - normal.major
        normal.minor = min(self.minor, flag)
        flag = flag - normal.minor
        normal.patch = min(self.patch, flag)
        if normal.alpha is not None:
            flag = flag - normal.patch
            normal.alpha = min(self.alpha, flag)

        return normal

    def unit_to_str(self) -> str:
        """ Returns the "string" based representation of a normalized tag
        (e.g. major, minor, patch). As an example, the result of this function on the SAHTag
        "v1.0.0" would be "major".
        """
        normal = self.normalize()
        return [t for t, f in zip(["major", "minor", "patch"],
                                  [normal.major, normal.minor, normal.patch]) if f][0]

    def rebase(self, new_base: str, new_letter: str = None) -> 'SAHTag':
        """ Keeps the semantic version numbering, but updates the base of the tag to the newly
        given base. It is also possible to update the tag letter.

        Args:
            new_base (str): The new base for the tag (e.g. master, proj_ap, etc.)
            new_letter (optional, str): The new letter (e.g. v, a, etc). Don't update if None is
                given, which is the default.

        Returns:
            SAHTag: returns a copy of itself with the updated base.
        """
        rebased = copy.copy(self)
        rebased.base = new_base
        if new_letter:
            rebased.letter = new_letter
        return rebased

    def _validate_base(self, other: 'SAHTag'):
        if self.get_branch() != other.get_branch() or self.letter != other.letter:
            raise ValueError("Cannot perform tag arithmetic on differently based tags! "
                             f"{str(self)} != {str(other)}")

    def __eq__(self, other: object) -> bool:
        return str(self) == str(other)

    def __ne__(self, other: object) -> bool:
        return not str(self) == str(other)

    def __le__(self, other: 'SAHTag') -> bool:
        self._validate_base(other)
        if self.major != other.major:
            return self.major < other.major
        if self.minor != other.minor:
            return self.minor < other.minor
        if self.patch != other.patch:
            return self.patch < other.patch
        self_alpha = self.alpha if self.alpha else 0
        other_alpha = other.alpha if other.alpha else 0
        if self_alpha != other_alpha:
            return self_alpha < other_alpha
        return True

    def __lt__(self, other: 'SAHTag') -> bool:
        self._validate_base(other)
        return self != other and self <= other

    def __ge__(self, other: 'SAHTag') -> bool:
        self._validate_base(other)
        if self.major != other.major:
            return self.major > other.major
        if self.minor != other.minor:
            return self.minor > other.minor
        if self.patch != other.patch:
            return self.patch > other.patch
        self_alpha = self.alpha if self.alpha else 0
        other_alpha = other.alpha if other.alpha else 0
        if self_alpha != other_alpha:
            return self_alpha > other_alpha
        return True

    def __gt__(self, other: 'SAHTag') -> bool:
        self._validate_base(other)
        return self != other and self >= other

    def __sub__(self, other: 'SAHTag') -> 'SAHTag':
        self._validate_base(other)
        new = copy.copy(self)
        new.major = max(0, self.major - other.major)
        new.minor = max(0, self.minor - other.minor)
        new.patch = max(0, self.patch - other.patch)
        new.alpha = None
        if self.alpha and other.alpha:
            new.alpha = max(0, self.alpha - other.alpha)
        return new

    def __add__(self, other: 'SAHTag') -> 'SAHTag':
        self._validate_base(other)
        new = copy.copy(self)
        new.major = self.major + other.major
        new.minor = self.minor + other.minor
        new.patch = self.patch + other.patch
        if self.alpha and other.alpha:
            new.alpha = max(0, self.alpha + other.alpha)
        return new

    def __mul__(self, other: 'SAHTag') -> 'SAHTag':
        self._validate_base(other)
        new = copy.copy(self)
        new.major = self.major * other.major
        new.minor = self.minor * other.minor
        new.patch = self.patch * other.patch
        if self.alpha and other.alpha:
            new.alpha = max(0, self.alpha * other.alpha)
        return new
