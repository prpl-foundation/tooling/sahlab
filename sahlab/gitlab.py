""" SAHLab Gitlab connection functionality """
import atexit
import os

from gitlab import Gitlab as PythonGitlab
import requests
from sahlab.singleton import Singleton
from sahlab.utilities import determine_gitlab_api_key


_DEFAULT_INSTANCE = os.environ.get("SAHLAB_DEFAULT_GIT_INSTANCE") or \
                    "https://gitlabinternal.softathome.com"


def _get_default_instance():
    """ Return the default instance in use. Is influenced by use_instance.
    """
    return _DEFAULT_INSTANCE


# pylint: disable=R0903 # Too few public methods
class Gitlab(metaclass=Singleton,
             key='instance',
             dyn_default=_get_default_instance):
    """ Encapsulation of the gitlab.Gitlab object. Sets up connection automatically with desired
    instance. Defaults to https://gitlabinternal.softathome.com. The API token used, will by default
    come from the environment variable 'GITLAB_API_KEY' for gitlabinternal, 'GITLAB_API_KEY_EXT'
    for gitlab.com and 'GITLAB_API_KEY_gitlab_instance' for any other gitlab instance.

    Direct use of this class should be avoided. Please try to use the concepts offered by sahlab
    (like the repository types) first and only use the direct gitlab connection as a last resort."""
    def __init__(self,
                 instance: str = "",
                 api_token: str = None):

        self.instance = instance if instance else _get_default_instance()
        api_token = api_token or determine_gitlab_api_key(self.instance)
        session = requests.Session()
        http_adapter = requests.adapters.HTTPAdapter(pool_connections=64,
                                                     pool_maxsize=64, max_retries=3)
        session.mount(self.instance, http_adapter)
        self.gitlab = PythonGitlab(self.instance, private_token=api_token, session=session,
                                   retry_transient_errors=True)


def use_instance(instance: str = "https://gitlabinternal.softathome.com",
                 api_token: str = None) -> Gitlab:
    """ Switches the default used Gitlab instance to the given instance. The corresponding Gitlab
    connection is returned for convenience. If the connection was not yet established, then it
    is created using the given api_token or the default from the environment ('GITLAB_API_KEY').

    Switching instance is not thread safe! Use multiprocessing to instantiate the multiple
    connections if parallel execution is desired.

    If no instance string is given, then the connection switches to the default at:
    https://gitlabinternal.softathome.com. """
    global _DEFAULT_INSTANCE  # pylint: disable=W0603 # Using global
    _DEFAULT_INSTANCE = instance
    return Gitlab(instance, api_token)


@atexit.register
def _close_connection():
    # TODO this maintenance task does some nasty stuff by looping over those instances
    # Maybe provide a function to get all established connections?
    for connection in Gitlab._instances.values():  # pylint: disable=protected-access
        if isinstance(connection, Gitlab):
            connection.gitlab.session.close()
