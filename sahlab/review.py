""" Reviews (MR) functionality """
import asyncio
import logging
import time
from typing import Any

from gitlab.exceptions import GitlabMRClosedError
from sahlab.gitlab import Gitlab


def truncate_description(dscr: str) -> str:
    """ Truncates the description if it exceeds the Gitlab maximum of 1048576 characters.
    A helpful message is appended at the end to explain why it has been truncated. """
    if len(dscr) > 1048576:
        dscr = dscr[:1048496] + \
            "\n[...]\n\n_MR Description has been truncated to not exceed maximum allowed length_"
    return dscr


class Review:
    """ The Review object (alternatively known as a Merge Request) provides all functionality
    to automatically setup MR or perform automated reviews.
    """
    def __init__(self, project, mr_obj):
        self.project = project
        self.mr_obj = mr_obj

    def get_attribute(self, attribute: str, refresh: bool = False,
                      include_diverged_commits_count: bool = False,
                      include_rebase_in_progress: bool = False) -> Any:
        """ Returns an attribute of the embedded python gitlab merge_request object.
        If refresh is True, the embedded merge_request object is re-created.
        The options include_diverged_commits_count and include_rebase_in_progress options determine
        the whether attributes corresponding to these features are returned or not. They are
        disabled by default and are only used if the option refresh is enabled as well.
        """
        if refresh:
            self.mr_obj = self.project.mergerequests\
                                .get(self.mr_obj.attributes["iid"],
                                     include_diverged_commits_count=include_diverged_commits_count,
                                     include_rebase_in_progress=include_rebase_in_progress)
        return self.mr_obj.attributes[attribute]

    def update_attributes(self, **kwargs: Any):
        """ Updates the given attributes in the kwargs dictionary.
        A given attribute must of course exist.
        Examples are title, description, labels, ...
        """
        if 'description' in kwargs:
            kwargs['description'] = truncate_description(str(kwargs['description']))
        list(map(lambda e: setattr(self.mr_obj, e, kwargs[e]), kwargs))
        self.mr_obj.save()

    def get_commits(self):
        """ Returns the commits in a merge request as a List[commits] python gitlab objects. """
        return self.mr_obj.commits()

    def get_pipelines(self):
        """ Returns all the pipelines of a merge request """
        return self.mr_obj.pipelines.list()

    async def wait_until_merged(self, timeout: int = 120, step_time: int = 10):
        """ Wait until the review has been merged or a timeout is reached. The review is polled
        in a fixed interval. """
        reference = self.get_attribute('references').get('full')
        time_max = time.time() + timeout
        while time.time() < time_max:
            if self.get_attribute("state", refresh=True) == "merged":
                return
            await asyncio.sleep(step_time)
        raise TimeoutError(f"Timeout exceeded while waiting for review {reference} to be merged!")

    async def wait_until_review_has_pipeline(self, sha: str = None,
                                             timeout: int = 120, step_time: int = 10):
        """ Wait until the review has a pipeline or a timeout is reached. The review is polled
        in a fixed interval. """
        reference = self.get_attribute('references').get('full')
        source = self.get_attribute("source_branch")
        sha = sha if sha else self.project.commits.get(source).attributes["id"]
        time_max = time.time() + timeout
        while time.time() < time_max:
            pipelines = self.mr_obj.pipelines.list()
            if pipelines and any(pipeline.sha == sha for pipeline in pipelines):
                return
            await asyncio.sleep(step_time)
        raise TimeoutError(f"Timeout exceeded while waiting on a pipeline for {reference}! ({sha})")

    async def _wait_until_review_can_be_merged(self, timeout: int = 90, step_time: int = 5):
        """ Wait until the merge_status of the Review is "can_be_merged".
        If this doesn't happen within the timeout, a TimeoutError is raised. """
        reference = self.get_attribute('references').get('full')
        time_max = time.time() + timeout
        while time.time() < time_max:
            if self.get_attribute("merge_status", refresh=True) == "can_be_merged":
                return
            await asyncio.sleep(step_time)
        raise TimeoutError(f"Timeout exceeded while waiting on merge status of {reference}!")

    async def merge_when_pipeline_succeeds(self, commit_message: str = None, retry: int = 0):
        """ Attempts to merge the MR when the pipelines succeeds. This will only work if the MR has
        a pipeline, otherwise, this will timeout.

        Args:
            commit_message (str, optional): Sets the commit message in case of a merge_commit merge

        Raises:
            TimeoutError: If a timeout occurs waiting for the pipeline to start or the MR to get
                in a merge-able state.
            GitlabMRClosedError: If the merge request is unable to be merged (Draft, Closed, ...)
            AssertionError: If merge when pipeline succeeds option was correctly set, but then
                immediately disabled. Note that script cannot detect the difference between a manual
                disable (intended) and an automatic disabled (not-intended).
        """
        sha = self.project.commits.get(self.get_attribute("source_branch")).attributes["id"]
        await self.wait_until_review_has_pipeline(sha=sha)
        await self._wait_until_review_can_be_merged()
        try:
            self.mr_obj.merge(merge_commit_message=commit_message,
                              merge_when_pipeline_succeeds=True,
                              sha=sha)
            # Gitlab sometimes handles events in an incorrect order, causing auto-merging to be
            # incorrectly disabled, this validation attempt will correct this if commits are made
            # in short succession
            logging.info("Merge status set! - Babysitting setting in case Gitlab gets confused...")
            await asyncio.sleep(30)
            assert self.get_attribute("state", refresh=True) == ("merged", "closed") \
                   or self.get_attribute('merge_when_pipeline_succeeds')
        except (AssertionError, GitlabMRClosedError) as err:
            if self.get_attribute("state", refresh=True) in ("merged", "closed"):
                logging.warning("Attempted to merge, but MR is already merged/closed!")
                return
            logging.warning("Failed attempt to set merge_when_pipeline_succeeds")
            if retry:
                await asyncio.sleep(10)  # Wait for Gitlab to catch up
                await self.merge_when_pipeline_succeeds(commit_message, retry - 1)
            else:
                raise err

    def post_note(self, msg: str):
        """ Posts a new note for this Review

        The given message cannot be empty.
        """
        self.mr_obj.notes.create({"body": msg})

    # TODO more MR Interaction like posting discussions

    def close(self):
        """ Closes this MR """
        self.mr_obj.state_event = 'close'
        self.mr_obj.save()

    def delete(self):
        """ Deletes this MR """
        self.mr_obj.delete()


def get_review_by_id(project_id: int, review_iid: int) -> Review:
    """ Creates a Review object based on the internal identifiers used in gitlab.

    Args:
        project_id (int): The ID of the gitlab project ('CI_PROJECT_ID').
        review_iid (int): internal MR ID of the gitlab project ('CI_MERGE_REQUEST_IID').

    Returns:
        Review: object matching given MR identifiers.
    """
    gitlab = Gitlab()
    project = gitlab.gitlab.projects.get(project_id)
    mr_obj = project.mergerequests.get(review_iid)
    return Review(project, mr_obj)


def get_review_by_reference(reference: str) -> Review:
    """ Creates a Review object based on the 'full' reference to it.
    Such a reference is formatted as follows: '<groups>/<project>!<mr_iid>'.

    This reference can be retrieved from a Review using the method:
    review.get_attribute("references").get("full")

    Args:
        reference (str): The 'full' reference to the MR.

    Returns:
        Review: object matching given MR reference.
    """
    gitlab = Gitlab()
    split_ref = reference.rsplit("!", 1)
    project = gitlab.gitlab.projects.get(split_ref[0])
    mr_obj = project.mergerequests.get(int(split_ref[1]))
    return Review(project, mr_obj)
