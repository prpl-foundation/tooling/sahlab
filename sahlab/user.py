""" User interaction API """
from typing import cast, List

from gitlab.v4.objects import CurrentUser
from gitlab.exceptions import GitlabGetError
from sahlab.gitlab import Gitlab


def _transform_user_id(username: str, strict: bool) -> int:
    gitlab = Gitlab()
    if " " in username:
        # Dealing with a Full name
        users = filter(lambda u: u.name.lower() == username.lower(),
                       gitlab.gitlab.users.list(all=True, per_page=100, iterator=True))
    else:
        users = filter(lambda u: not strict or u.username == username,
                       gitlab.gitlab.users.list(search=username, all=True, iterator=True))
    users_list = list(users)
    if len(users_list) != 1:
        return 0
    return users_list[0].id


def list_user_ids(usernames: List[str], strict: bool = True) -> List[int]:
    """ Transforms the given list of gitlab usernames or full names to a list of integers (ids).
    If the user is not found or strict is set and multiple users are found, 0 is returned instead.
    Attempting to use this user id will result in a User not found error."
    """
    return list(map(lambda u: _transform_user_id(u, strict), usernames))


def get_current_user() -> CurrentUser:
    """ Returns information about the current user, based on GITLAB_API_KEY.
    """
    gitlab = Gitlab()
    gitlab.gitlab.auth()
    return cast(CurrentUser, gitlab.gitlab.user)


