""" Repository objects and helper methods """
# pylint: disable=too-many-lines
# TODO: split repository module
from collections import namedtuple
import os
import re
import time
from typing import Any, Dict, List, Optional, Tuple, Type, Union
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from urllib.error import HTTPError
import yaml

from gitlab.exceptions import GitlabDeleteError, GitlabGetError
from sahlab.changelog import categorize
from sahlab.exceptions import RefGetError, FileGetError
from sahlab.gitlab import Gitlab
from sahlab.haystack import Haystack
from sahlab.issue import Issue, JiraIssue, issue_extract
from sahlab.naming import Name, NameFactory, SAHTag, NameBuilder
from sahlab.pipeline import Pipeline
from sahlab.review import Review, truncate_description
from sahlab.user import list_user_ids
from sahlab.utilities import get_unix_epoch_time, master_or_main

Author = namedtuple('Author', 'name email')
Author.__doc__ = """Immutable tuple representing a commit author pair (name and email) """
Author.name.__doc__ = """The name of the commit author """
Author.email.__doc__ = """The email address of the commit author """


# pylint: disable=too-many-instance-attributes
class CommitBuilder():
    """ A Git repository consists of a commit tree. Any update to a repository comes in the form of
    a commit. This class provides an easy way to construct your new commit dynmically. It can then
    be passed to the SAHRepo instance to be applied.
    """
    def __init__(self):
        self.commit_msg = None
        self.target_branch = None

        self.start_branch = None
        self.start_sha = None

        self.author = Author("sahbot", "sahbot@softathome.com")

        self.actions = []
        self.force = False
        self.signoff = False

    def with_commit_message(self, commit_msg: str) -> 'CommitBuilder':
        """ Update the commit message. """
        self.commit_msg = commit_msg
        return self

    def with_target_branch(self, target_branch: str) -> 'CommitBuilder':
        """ Update the target branch. """
        self.target_branch = target_branch
        return self

    def from_branch(self, start_branch: str) -> 'CommitBuilder':
        """ New branch should branch of the given branch.
        This cancels out any previous `sahlab.repository.CommitBuilder.from_branch` or
        `sahlab.repository.CommitBuilder.from_commit` calls. """
        self.start_sha = None
        self.start_branch = start_branch
        return self

    def from_commit(self, start_sha: str) -> 'CommitBuilder':
        """ New branch should branch of the given commit (sha).
        This cancels out any previous `sahlab.repository.CommitBuilder.from_branch` or
        `sahlab.repository.CommitBuilder.from_commit` calls. """
        self.start_sha = start_sha
        self.start_branch = None
        return self

    def with_author(self, name: str, email: str) -> 'CommitBuilder':
        """ Updates the author information (name and email pair).
        If this isn't configured, the default sahbot information will be used. """
        self.author = Author(name, email)
        return self

    def update(self, file_path: str, content: str) -> 'CommitBuilder':
        """ This commit should update the given file to the given content. """
        self.actions.append({"action": "update",
                             "file_path": file_path,
                             "content": content})
        return self

    def create(self, file_path: str, content: str) -> 'CommitBuilder':
        """ This commit should create the given file with the given content. """
        self.actions.append({"action": "create",
                             "file_path": file_path,
                             "content": content})
        return self

    def delete(self, file_path: str) -> 'CommitBuilder':
        """ This commit should delete the given file. """
        self.actions.append({"action": "delete", "file_path": file_path})
        return self

    def build(self) -> dict:
        """ Converts the gathered information and actions into a dict structure that is accepted by
        the Gitlab API. At least one action should be defined. """
        if not self.commit_msg or not self.target_branch:
            raise ValueError("A commit should have a message and target branch!")
        if not self.actions:
            raise ValueError("At least one action should be specified for a commit!")
        data = {"commit_message": self.commit_msg + f"\n\nSigned-off-by: {self.author.name} \
                <{self.author.email}>" if self.signoff else self.commit_msg,
                "branch": self.target_branch,
                "actions": self.actions,
                "author_email": self.author.email,
                "author_name": self.author.name,
                "force": self.force}
        if self.start_branch:
            data["start_branch"] = self.start_branch
        if self.start_sha:
            data["start_sha"] = self.start_sha
        return data


# pylint: disable=too-many-public-methods
class SAHRepo():
    """ Base SAHRepo class. This abstract class should not be used directly,
    instead one of its subclasses should be used as they will provide the correct functionality to
    retrieve or update requested information. An example for this would be how finding dependencies
    is handled between configs, metas or other types of projects.

    Common functionality will be defined here to reduce code duplication.
    """
    def __init__(self, definition: dict, project=None):
        self.definition = definition
        self.gitlab = Gitlab()
        if project:
            self.project = project
            self._lazy = False
        else:
            self.project = self.gitlab.gitlab.projects.get(definition['id'], lazy=True)
            self._lazy = True
        self.wrapped = self.project
        self._yml_cache: Dict[str, Dict] = {}

    def __str__(self):
        return f'{self.__class__.__name__}(id={self.definition["id"]}, '\
               f'name={self.definition["namespace"]}/{self.definition["name"]})'

    def __eq__(self, other: object) -> bool:
        """ Two SAHRepo objects match if the nested 'id' match and the SAHRepo type match
        (needed for composite repos). """
        if not isinstance(other, SAHRepo):
            return False
        return self.definition["id"] == other.definition["id"] and self.__class__ == other.__class__

    def get_sha(self, ref: str) -> str:
        """ Get current commit hash for the given reference (branch/tag). """
        try:
            return self.project.commits.get(ref).attributes["id"]
        except GitlabGetError as error:
            raise RefGetError(self, ref) from error

    def _get_search_filter(self) -> str:
        return self.definition.get("search", fr"^{master_or_main()}$")


    def _parallel_parse_dep_files(self, dep_files: List[str],
                                  ref: str, sha: str) -> List['Dependency']:
        """ Parses multiple dependency files in parallel for improved performace.

        Args:
            dep_files (List[str]): A list of paths to dependency files
            ref (str): The reference of the file to use
            sha (str): The sha of the ref to include in the Dependency object

        Returns:
            List[Dependency]: Parsed dependencies
        """
        # TODO: refactor to parallel module
        with ThreadPoolExecutor(max_workers=16, thread_name_prefix='get_dependencies') as executor:
            futures = [(dep_file, executor.submit(self._parse_dep_file, dep_file, sha))
                       for dep_file in dep_files]

        dependencies = []
        for future in futures:
            resolved_deps = future[1].result()
            try:
                dependencies.extend([Dependency(dep[0], dep[1], future[0], self, ref, sha)
                                     for dep in resolved_deps])
            except ValueError as err:
                raise ValueError("Dependency not resolving correctly, probably found "
                                 f"wrong format file in {self}") from err
        return dependencies


    def _do_find_dependency(self, component: str,
                            branch_filter: str = "",
                            tag_filter: str = "") -> List['Dependency']:
        found_dependencies = []
        # Check branches
        if branch_filter is not None:
            branch_filter = branch_filter if branch_filter else self._get_search_filter()
            valid_refs = filter(lambda ref: bool(re.match(branch_filter, ref.attributes["name"])),
                                self.project.branches.list(all=True))
            for ref in valid_refs:
                matched_deps = [dep for dep in self.get_dependencies(ref.attributes["name"])
                                if component == dep.name]
                found_dependencies.extend(matched_deps)
        # Check tags
        if tag_filter:
            valid_refs = filter(lambda ref: bool(re.match(tag_filter, ref.attributes["name"])),
                                self.project.tags.list(all=True))
            for ref in valid_refs:
                matched_deps = [dep for dep in self.get_dependencies(ref.attributes["name"])
                                if component == dep.name]
                found_dependencies.extend(matched_deps)
        return found_dependencies


    def get_attribute(self, attribute: str) -> Any:
        """ Returns an attribute of the embedded gitlab project. If the project was loaded lazily,
        it will first be fetched (via an API call) before the full list of attributes is available.
        """
        if self._lazy:
            # Get full object if still lazy
            self.project = self.gitlab.gitlab.projects.get(self.definition['id'])
            self._lazy = False
            self.wrapped = self.project
        return self.project.attributes[attribute]

    def update_attributes(self, **kwargs: Any):
        """ Updates the given attributes in the kwargs dictionary.
        A given attribute must of course exist.
        """
        list(map(lambda e: setattr(self.project, e, kwargs[e]), kwargs))
        self.project.save()

    def get_raw_file(self, path: str, ref: str = "") -> str:
        """ Returns the raw contents of the requested file. The file contents are 'utf-8' encoded
        (i.e. shouldn't be decoded to use with regular python strings). The default branch is used
        if no ref is given. """
        ref = ref if ref else self.get_attribute("default_branch")
        try:
            return self.project.files.raw(path, ref=ref).decode("utf-8")
        except GitlabGetError as error:
            raise FileGetError(self, ref, path) from error

    def has_file(self, path: str, ref: str = "") -> bool:
        """ Returns True if this repo has a file at the given path on the given ref,
        False otherwise. The default branch is used if no ref is given. """
        ref = ref if ref else self.get_attribute("default_branch")
        try:
            self.project.files.get(path, ref=ref)
            return True
        except GitlabGetError:
            return False

    def parse_yml(self, file: str, path: str = "", ref: str = "", default: Any = None) -> Any:
        """ Parses the given yml file to get the value on the given path. If no path
        is given, the whole yml file is returned in a dictionary structure.
        If the path or the file doesn't exist, it returns the given default value.

        Imagine you have the following yml file (example.yml) in your repository:
        ---
        A:
          E: "string"
          L:
            - "Item 1"
            - "Item 2"
        ---
        You could retrieve the element "E" by calling parse_yml with the filename "example.yml" and
        the path "A.E". This works with other data structures as well, like bools, lists or dicts.
        Passing the path "A.L" would return a list containing two strings.
        While it is possible to navigate arbitrarily deep in a nest of dictionaries, it is not
        possible to filter out a specific entry of a list using this function.
        """
        ref = ref if ref else self.get_attribute("default_branch")
        if not self._yml_cache.get(file):
            self._yml_cache[file] = {}
        if not self._yml_cache[file].get(ref):
            try:
                self._yml_cache[file][ref] = yaml.load(self.get_raw_file(file, ref=ref),
                                                       Loader=yaml.BaseLoader)
            except GitlabGetError:
                # File or ref does not exist
                return default
        value = self._yml_cache[file][ref]

        if not path:
            return value

        for elem in re.split(r'(?<!\\)\.', path):
            elem = elem.replace(r'\.', '.')
            try:
                value = value[elem]
            except (KeyError, TypeError, IndexError):
                return default
        return value

    def get_owners(self, ref: str = "default", updated_files: List[str] = None) -> List[str]:
        """ Returns the owner of this repo (potentially influenced by the given branch). If not
        defined at repo creation, then the returned list will be empty.

        Additionally, ownership might be tied to certain files. It is up to the SAHRepo subclass to
        use this information intelligently. It is thus ignored for the base SAHRepo implementation.

        Args:
            ref (str, optional): The reference for which to list the owners. Defaults to "default".
            updated_files (List[str], optional): A list of updated dependency files to determine
                owner of, unused by the base implementation. Defaults to None.

        Returns:
            List[str]: A list of usernames that "own" the requested branch/updated_files of this
                repo.
        """
        del updated_files  # unused
        if "owners" not in self.definition:
            return []
        owners = self.definition["owners"][ref] if ref in self.definition["owners"]\
            else self.definition["owners"]["default"]
        return owners.split()

    def create_commit(self, commit_builder: CommitBuilder):
        """ Creates a new commit based on the configured changes defined by the given CommitBuilder.
        A commit should have a defined commit message, target reference and at least one action.

        The new commit is returned as a python-gitlab object.
        """
        return self.project.commits.create(commit_builder.build())

    def create_branch(self, new_branch: str, from_ref: str):
        """ Creates a new branch from a given ref (branch, tag, sha). """
        self.project.branches.create({"branch": new_branch, "ref": from_ref})

    def has_branch(self, branch: str) -> bool:
        """ Checks if this repo has a given branch, returns True if that is the case,
        False otherwise. """
        return any(b.name == branch for b in self.project.branches.list(search=branch,
                                                                        iterator=True))

    def delete_branch(self, branch: str, retry: int = 3):
        """ Removes a branch if it exists. This has as side effect that all MR for that branch
        are automatically closed.
        """
        try:
            self.project.branches.delete(branch)
        except GitlabDeleteError as err:
            if retry:
                time.sleep(1)
                self.delete_branch(branch, retry - 1)
            else:
                raise err

    def protect_branch(self, name: str, merge_access_level: int, push_access_level: int):
        """ Sets a branch or branch-filter as protected. Access-level can be specified
        through the merge_access_level and push_access_level variables.
        """
        try:
            self.project.protectedbranches.delete(name)
        except GitlabDeleteError:
            # protected branch did not exist yet.
            pass
        self.project.protectedbranches.create({"name": name,
                                               "merge_access_level": merge_access_level,
                                               "push_access_level": push_access_level})

    def protect_tag(self, name: str, create_access_level: int):
        """ Sets a tag or tag-filter as protected. Access-level can be specified
        through the create_access_level variable.
        """
        try:
            self.project.protectedtags.delete(name)
        except GitlabDeleteError:
            # protected tag did not exist yet.
            pass
        self.project.protectedtags.create({"name": name,
                                           "create_access_level": create_access_level})

    def list_branches(self, branch_filter: str = "", protected: bool = False) -> List[str]:
        """ List all branches of a project.

        Args:
            branch_filter (str, optional): Regular expression filter str
            protected (bool, optional): Filter to only include protected branches
        """
        branches = self.project.branches.list(all=True)
        if protected:
            branches = [branch for branch in branches if branch.attributes["protected"]]
        return [branch.name for branch in branches
                if bool(re.match(branch_filter, branch.attributes["name"]))]

    def create_tag(self, new_tag: SAHTag, msg: str = "", release: str = ""):
        """ Creates a new tag based on a given SAHTag object. Optional tag message and release
        notes can be set. The new tag will be placed on the branch corresponding with the base of
        the given SAHTag.

        Args:
            new_tag (SAHTag): The new tag
            msg (str, optional): Tag msg
            release (str, optional): Release notes to attach to the tag
        """
        self.project.tags.create({"tag_name": str(new_tag), "ref": new_tag.get_branch(),
                                  "message": msg})
        if release:
            self.project.releases.create({"name": str(new_tag), "tag_name": str(new_tag),
                                          "description": release})

    def get_categorized_commits_between(self, from_ref: str, to_ref: str,
                                        unique: bool = True,
                                        sort: bool = True) -> Dict[str, List[str]]:
        """ Parses all commit titles between the given references.
        Every commit that has a category suffix (e.g. [fix], [new], [change], ...) is selected.
        The returned dictionary maps all of these lists of commit titles to their respective
        category. It is possible that a given category doesn't exist as no such commit was found
        between the refs.
        """
        comp = self.project.repository_compare(from_ref, to_ref)
        commits = []
        for commit in comp['commits']:
            commits.extend(list(map(str.strip, commit.get("message").strip().split('\n'))))

        if unique:
            commits = list(set(commits))
        if sort:
            commits.sort()
        return categorize(commits, discard=True)

    def get_issues_between(self, from_ref: str, to_ref: str,
                           unique: bool = True, sort: bool = True,
                           default_dummy: bool = False, plain_issue: bool = False) -> List[Issue]:
        """ List all handled issues between two references.
        Issues are collected based on the commit titles, so the formatting of the commit
        message is important for the automated tool to correctly extract the relevant information.
        Some simple shaping of the list is provided including reducing to unique issues and sorting.

        Args:
            from_ref (str): The starting reference for the comparison
            to_ref (str): The target reference for the comparison (from_ref < target_ref)
            unique (bool, optional): Specifies if the list should be reduced to unique issues.
                Defaults to True.
            sort (bool, optional): Specifies if the list should be sorted. Defaults to True.
            default_dummy (bool, optional): Specifies if a dummy issue should be returned if no
                other issue can be found. Defaults to False.
            plain_issue (bool, optional): If True, no connection with gitlab/jira is made
                in the background. Useful when no operations on the issues need to be done.
                False by default.

        Returns:
            List[Issue]: List of all found Issues between the given references. Note that this list
                will be in a random order if unique is set to True and sort is set to False.
        """
        comp = self.project.repository_compare(from_ref, to_ref)
        issues = []
        # Look for Jira Tickets
        for commit in comp['commits']:
            try:
                issues.extend(issue_extract(commit['message'], default_dummy=False,
                                            origin=self.get_attribute('path_with_namespace'),
                                            plain_issue=plain_issue))
            except (ValueError, HTTPError):
                pass
        if not issues and default_dummy:
            issues.append(JiraIssue.dummy())

        # TODO: Look for Gitlab Issues closed between two refs
        if unique:
            issues = list(set(issues))
        if sort:
            issues.sort()
        return issues

    def get_review(self, source_branch: str, target_branch: str) -> Optional[Review]:
        """ Returns any (open) pre-existing MR from the source branch to the target branch
        if it exists. Returns None if no MR is found. """
        mrs = self.project.mergerequests.list(state='opened',
                                              source_branch=source_branch,
                                              target_branch=target_branch)
        return Review(self.project, mrs[0]) if mrs else None

    # pylint: disable=R0913 # Creation of MR is very configuration heavy,
    #                         grouping in tuples or objects doesn't seem to be the way to go
    #                         (for now)
    def create_mr(self, source_branch: str, target_branch: str,
                  title: str, description: str = "", labels: List[str] = None,
                  assignee_ids: List[int] = None, update_existing: bool = True) -> Review:
        """ Create a new MR from a given source branch to a given target branch.
        An MR should have a title. Optionally, a larger description can be given as well.
        If no specific assignees are given, the default owners of the repo will be chosen as
        assignee. This ownership information is encoded in the Haystack definition of a project.

        Args:
            source_branch (str): The from or source branch for the MR
            target_branch (str): The to or target branch for the MR
            title (str): The title of the MR
            description (str, optional): The body of the MR, defaults to empty. If the description
                length exceeds the maximum of Gitlab (1048576 characters), then it is truncated.
            labels (List[str], optional): Additional labels to attach to the MR.
            assignee_ids (List[int], optional): A list of user identifiers that the MR should be
                assigned to. If empty or None, the owners of the target branch of this repo are
                assigned.
            update_existing (bool, optional): If True, updates any pre-existing MR for the given
                source/target branch combination. The title, description and assignees is updated
                to reflect the newly given values. Defaults to True.

        Raises:
            ValueError: When a MR already exists for the given source/target branch combination and
                update_existing is set to False.
        """
        review = self.get_review(source_branch, target_branch)
        labels = labels or []
        assignee_ids = assignee_ids or list_user_ids(self.get_owners(target_branch))
        assignee_ids = list(set(assignee_ids))  # They need to be unique
        if review:
            if not update_existing:
                raise ValueError(f"MR for {str(self)} from {source_branch} to "
                                 f"{target_branch} already exists!")
            review.update_attributes(title=title,
                                     description=description,
                                     assignee_ids=assignee_ids,
                                     labels=labels)
            return review
        description = truncate_description(description)
        mr_obj = self.project.mergerequests.create({"source_branch": source_branch,
                                                    "target_branch": target_branch,
                                                    "title": title,
                                                    "description": description,
                                                    "remove_source_branch": True,
                                                    "labels": labels,
                                                    "assignee_ids": assignee_ids})
        return Review(self.project, mr_obj)

    def get_previous_tag(self, tag: SAHTag, ignore_a_tags: bool = False) -> Optional[SAHTag]:
        """ Returns the SAHTag corresponding with the one preceding the given reference.
        This has its use cases in determining the difference between a new/current tag and the
        previous one. As SAHTags can follow multiple branches, the previous one will match the
        same line as the given one.

        If the given tag is the first tag in the series, None will be returned.
        """
        searchstr = "^" + tag.get_branch() + "_"
        all_tags = self.project.tags.list(all=True, search=searchstr)
        if tag.get_branch() == master_or_main():
            all_tags += self.project.tags.list(all=True, search="^v")

        # Gitlab is in some rare cases not returning the tags in the correct order (by date)
        all_tags = sorted(all_tags, key=lambda x: get_unix_epoch_time(x.commit["created_at"]),
                          reverse=True)

        all_tags = [t.name for t in all_tags if SAHTag.validate(t.name)
                    and SAHTag(t.name).get_branch() == tag.get_branch()]
        if ignore_a_tags:
            all_tags = list(filter(lambda t: SAHTag(t).letter != "a", all_tags))

        index = all_tags.index(str(tag))
        if index == len(all_tags) - 1:
            # Given tag is earliest tag
            return None
        return SAHTag(all_tags[index + 1])

    def get_last_tag(self, branch: str, ignore_a_tags: bool = False) -> Optional[SAHTag]:
        """ Returns the latest SAHTag on a specific branch.
        If no tag exists yet for that branch, then None will be returned.
        v tags are equal to v tags.
        """
        searchstr = "^" + branch + "_"
        all_tags = self.project.tags.list(all=True, search=searchstr)
        if branch == master_or_main():
            all_tags += self.project.tags.list(all=True, search="^v")

        # Gitlab is in some rare cases not returning the tags in the correct order (by date)
        all_tags = sorted(all_tags, key=lambda x: get_unix_epoch_time(x.commit["created_at"]),
                          reverse=True)

        all_tags = [tag.name for tag in all_tags]
        for tag in all_tags:
            try:
                sahtag = SAHTag(tag)
            except (ValueError, AssertionError):
                continue
            if sahtag.get_branch() == branch and (not ignore_a_tags or sahtag.letter != "a"):
                return sahtag
        return None

    def create_pipeline(self, ref: str = "", variables: dict = None) -> Pipeline:
        """ Run a new pipeline for this repository for a given reference.
        The default branch is used if no ref is given.
        It is possible to pass extra variables in the form of a dictionary.
        All additional variables must have string keys and values. """
        ref = ref if ref else self.get_attribute("default_branch")
        create_params: Dict[str, Union[str, List[Dict[str, str]]]] = {"ref": ref}
        if variables:
            create_params['variables'] = [{'key': str(i[0]), 'value': str(i[1])}
                                          for i in variables.items()]
        return Pipeline(self.project.pipelines.create(create_params))

    def get_pipeline_by_id(self, pipeline_id: int) -> Pipeline:
        """ Return a `sahlab.pipeline.Pipeline` object from a pipeline id. """
        return Pipeline(self.project.pipelines.get(pipeline_id))

    def get_last_pipeline(self, review: Review = None, ref: str = "") -> Optional[Pipeline]:
        """ Return the latest pipeline for a given Review (MR) or branch (mutually exclusive).
        Returns None, if no pipeline has been run for the given ref. """
        if review:
            pipelines = review.mr_obj.pipelines.list(per_page=1, get_all=True)
            return self.get_pipeline_by_id(pipelines[0].id) if pipelines else None
        pipelines = self.project.pipelines.list(order_by='id', sort='desc', ref=ref, per_page=1,
                                                get_all=True)
        return Pipeline(pipelines[0]) if pipelines else None

    def get_pipelines_for_sha(self, sha: str) -> List[Pipeline]:
        """ Return a list of 'sahlab.pipeline.Pipeline' objects that ran for a specific sha.
        Note that this list can contain pipelines for branches, MR and tags! """
        pipelines = self.project.pipelines.list(order_by='id', sha=sha, iterator=True)
        return [Pipeline(pipeline) for pipeline in pipelines]

    def create_pipeline_schedule(self, ref: str,
                                 description: str, cron: str, variables: dict = None):
        """
        Create a pipeline-schedule. Pass the optional variables to the pipeline-schedule.

        Args:
            ref (str): The reference to check for dependencies. A reference could refer
                to a branch, but also to a tag.
            description (str): The description of the pipeline schedule.
            cron (str): The cron schedule, for example: 0 1 * * *.
            variables (dict): Dictionary of all the variables you want to setup for the
                pipeline. For example: {'MIRROR_BRANCHES': 'main', 'MIRROR_USER': 'sahbot'
        """
        pipeline_sched_configuration = {'ref': ref,
                                        'description': description,
                                        'cron': cron}
        schedule = self.project.pipelineschedules.create(pipeline_sched_configuration)
        if variables is None:
            return schedule
        for variable in variables:
            schedule.variables.create({'key': variable, 'value': variables[variable]})
        return schedule

    def is_merged(self, ref: str) -> bool:
        """ Returns True if the given branch is merged. False otherwise. """
        if not self.has_branch(ref):
            return False
        return self.project.branches.get(ref).merged

    # pylint: disable=no-self-use
    def get_haystack_integration_query(self) -> str:
        """ Returns the selection part of the jq query regarding which haystack sections are
        interesting for this SAHRepo type.
        """
        return '.config,.meta,.yocto,."openwrt-feed",.container,."gitlab-ci"'


class PlainRepo(SAHRepo):
    """ SAHRepo variant for an assortment of as of yet uncategorized repository types.
    This repo type cannot look for dependencies, but all other standard interaction is still
    possible. This way you can still easily fetch files, open reviews, make commits, etc. on
    every repo of choice.
    """


class MetaRepo(SAHRepo):
    """ SAHRepo variant for "meta" repositories.
    """


class ConfigRepo(SAHRepo):
    """ SAHRepo variant for "config" repositories.

    """


class WrtConfigRepo(SAHRepo):
    """ SAHRepo variant for "wrt config" repositories.
    """


class PrplConfigRepo(SAHRepo):
    """ SAHRepo variant for "prpl config" repositories.

    This kind of repos contain the buildsystem, but also some configuration stored in
    the profiles directory. An example is the prplos repository on gitlab.com.
    """
    def _parse_dep_file(self, dep_file: str, ref: str) -> List[Tuple]:
        deps: List[Tuple] = []
        feeds = self.parse_yml(dep_file, path="feeds", ref=ref, default=[])

        for feed in feeds:
            search = re.search(r"([^@]+)@[a-f0-9]{40}", feed["revision"])
            revision = None
            if search:
                revision = search.group(1)
            deps.append((feed["name"], revision if revision else feed["revision"]))
        return deps

    def get_dependencies(self, ref: str) -> List['Dependency']:
        sha = self.get_sha(ref)
        profiles = [hit["path"] for hit in self.project.repository_tree(ref=sha,
                                                                        recursive=True,
                                                                        per_page=100,
                                                                        path="profiles/",
                                                                        all=True)
                    if hit['path'].startswith("profiles/")]
        return self._parallel_parse_dep_files(profiles, ref, sha)

    def find_dependency(self, component: Name,
                        branch_filter: str = "",
                        tag_filter: str = "") -> List['Dependency']:
        return self._do_find_dependency(component=component.openwrt_name,
                                        branch_filter=branch_filter,
                                        tag_filter=tag_filter)

    def update_dependency(self, dependency: 'Dependency',
                          version: str, opensource_version: str = "",
                          integration_branch: str = "") -> str:
        dep_file_contents = self.get_raw_file(dependency.found_in,
                                              ref=integration_branch or dependency.dependant_sha)

        pattern = rf'- name: {dependency.name}\n(?:\s+.+?: .+?\n)*?\s+uri: https://.*?/(.*)'
        search = re.search(pattern, dep_file_contents)

        # try to get the sha of the given version of the dependency
        version_sha = None
        if search:
            dependency_repo_path = search.group(1).replace(".git", "")
            depdency_repo_name = NameBuilder().with_path(dependency_repo_path).build()
            try:
                dependency_repo = repo_create_by_name(depdency_repo_name, class_hint=PlainRepo)
                version_sha = dependency_repo.get_sha(version)
            except GitlabGetError:
                pass

        pattern = rf'(- name: {dependency.name}\n(?:\s+.+?: .+?\n)*?\s+revision: ).*'
        return re.sub(pattern,
                      r'\1' + version + f"@{version_sha}" if version_sha else r'\1' + version,
                      dep_file_contents)


class YoctoLayerRepo(SAHRepo):
    """ SAHRepo variant for "Yocto Layer" components type repositories.

    """


class YoctoDistroRepo(SAHRepo):
    """ SAHRepo variant for "Yocto Layer" distro type repositories.

    """


class WrtBuildsystemRepo(SAHRepo):
    """ SAHRepo variant for the "OpenWRT buildsystem" repositories.

    """


class KernelRepo(SAHRepo):
    """ SAHRepo variant for cssp kernel repositories.

    """


class OpenwrtFeedRepo(SAHRepo):
    """ SAHRepo variant for the "OpenWRT Feed" repositories.

    An OpenWRT Feed defines packages for a collection of components. Components are organised
    in Categories and Sections, but can be placed virtually anywhere in the repository. Each package
    must have its own directory though (named as the package) with at least a Makefile.

    The package links back to its source using the SAH_URL variable. The version used is defined in
    the SAH_VERSION variable.
    """
    def _get_search_filter(self) -> str:
        return self.definition.get("search", fr"^{master_or_main()}$|^gen_|^proj_|^lantiq_gen_")

    def get_dependencies(self, ref: str) -> List['Dependency']:
        sha = self.get_sha(ref)
        packages = [hit['path'] for hit in self.project.repository_tree(ref=ref,
                                                                        recursive=True,
                                                                        per_page=100,
                                                                        all=True)
                    if re.match(r'.*/Makefile$', hit['path'])]
        dependencies = []
        for package in packages:

            version_search = re.search(r'[^\#](PKG|SAH)_VERSION:=(.*)',
                                       self.get_raw_file(package, ref=sha))
            dependencies.append(Dependency(package.split("/")[-2],
                                           version_search.group(2) if version_search
                                           else master_or_main(),
                                           package,
                                           self,
                                           ref,
                                           sha))
        return dependencies

    def update_dependency(self, dependency: 'Dependency',
                          version: str, opensource_version: str = "",
                          integration_branch: str = "") -> str:
        dep_file_contents = self.get_raw_file(dependency.found_in,
                                              ref=integration_branch or dependency.dependant_sha)
        ret = re.sub(r'PKG_(SOURCE_)?VERSION(:)?=(.*)',
                     rf'PKG_\1VERSION\2={version}',
                     dep_file_contents)
        ret = re.sub(r'PKG_HASH:=.*', 'PKG_HASH:=skip', ret)

        return re.sub(r'PKG_(SOURCE_)?VERSION(:)?=(.*)',
                      rf'PKG_\1VERSION\2={opensource_version}',
                      ret) if opensource_version else ret

    def find_dependency(self, component: Name,
                        branch_filter: str = "",
                        tag_filter: str = "") -> List['Dependency']:
        return self._do_find_dependency(component=component.openwrt_name,
                                        branch_filter=branch_filter,
                                        tag_filter=tag_filter)

    # pylint: disable=no-self-use
    def get_haystack_integration_query(self) -> str:
        return '."openwrt-config",."prpl-config"'


class ContainerRepo(SAHRepo):
    """ SAHRepo variant for docker container repositories.

    """


class GitlabCIRepo(SAHRepo):
    """ SAHRepo variant for Gitlab CI yml template repositories.

    """


class PythonRepo(SAHRepo):
    """ SAHRepo variant for Python projects.

    """


class CompositeRepo(SAHRepo):
    """ Some repositories have multiple functions, for example the BAF repository is both a
    ContainerRepo (it has a Dockerfile), but it also defines the BAF python package (setup.py).
    It can thus be used as multiple different types at the same time.
    In order to prevent code duplication, this SAHRepo type will combine these multiple type
    instances and call their dependency related functions to return an aggregate.
    In order to seed the subtypes, these have to be defined in the passed 'definition' dictionary
    as a tuple under the key 'types' (normally seeded by repo_create).
    """
    def __init__(self, definition: dict, project=None):
        super().__init__(definition, project)
        if not CompositeRepo.validate_types(definition.get("types")):
            raise ValueError("CompositeRepo requires at least 2 valid SAHRepo subtypes to be passed"
                             f" in it's definition! Got {repr(definition.get('types'))}")
        self.subtypes = tuple(map(lambda t: t(definition, project), self.list_types()))

    def __str__(self):
        return f'CompositeRepo(id={self.definition["id"]}, '\
               f'name={self.definition["namespace"]}/{self.definition["name"]}, '\
               f'subtypes={[s.__class__.__name__ for s in self.subtypes]}'

    def __eq__(self, other: object) -> bool:
        """ Two CompositeRepo objects match if the nested 'id' match and the nested types match
        (needed for composite repos). """
        return isinstance(other, CompositeRepo) \
            and self.definition["id"] == other.definition["id"] \
            and self.list_types() == other.list_types()

    @staticmethod
    def validate_types(types: Optional[Tuple[Type[SAHRepo]]]) -> bool:
        """ Validates if the given types match the expectations to create a CompositeRepo. A valid
        set of types is passed as a tuple of SAHRepo types with at least 2 types. """
        return types is not None \
            and len(types) > 1 \
            and all(issubclass(typ, SAHRepo) for typ in types)

    def list_types(self) -> Tuple[Type[SAHRepo]]:
        """ Returns a tuple of the types this CompositeRepo is configured for.

        These subtypes are defined in the SAHRepo definition dictionary as a tuple under the key
        'types'. Normally these types are deduced by `sahlab.repository.repo_create` based on the
        information stored in the haystack. If an ID shows up in multiple sections, multiple types
        are associated.
        """
        return self.definition.get("types", ())

    def find_dependency(self, component: Name,
                        branch_filter: str = "",
                        tag_filter: str = "") -> List['Dependency']:
        dependencies = []
        for subtype in self.subtypes:
            dependencies.extend(subtype.find_dependency(component, branch_filter, tag_filter))
        return dependencies

    def get_dependencies(self, ref: str) -> List['Dependency']:
        sha = self.get_sha(ref)
        dependencies = []
        for subtype in self.subtypes:
            dependencies.extend(subtype.get_dependencies(sha))
        return dependencies

    def get_owners(self, ref: str = "default", updated_files: List[str] = None) -> List[str]:
        owners = []
        for subtype in self.subtypes:
            owners.extend(subtype.get_owners(ref, updated_files))
        return list(set(owners))  # Make unique

    def get_haystack_integration_query(self) -> str:
        targets = []
        for subtype in self.subtypes:
            targets.extend(subtype.get_haystack_integration_query().split(","))
        return ",".join(set(targets))

    def update_dependency(self, dependency: 'Dependency',
                          version: str, opensource_version: str = "",
                          integration_branch: str = "") -> str:
        try:
            index = self.subtypes.index(dependency.dependant)
        except ValueError as err:
            raise ValueError("Given dependency does not belong to this repo") from err
        return self.subtypes[index].update_dependency(dependency, version, opensource_version,
                                                      integration_branch)


def repo_create_by_id(repo_id: int, class_hint=None, haystack: Haystack = None) -> SAHRepo:
    """ Factory method for creating SAHRepo instances when only an identifier is available.

    This method will fetch the remaining information directly from gitlab and forward it to the
    `sahlab.repository.repo_create` function. The rest of the type inference logic is identical.
    Similarly, you can also provide a class_hint to be forwarded as well.

    There is no possibility to encode ownership information of a repository this way.

    Note that this way of creating repositories is significantly slower than providing all the
    required information directly to `sahlab.repository.repo_create`, as more API calls are needed.

    Args:
        repo_id (int): The identifier of the gitlab project.
        class_hint (class, optional): Class hint to skip type inference in
            `sahlab.repository.repo_create`.
        haystack (Haystack, optional): A sahlab haystack object used to find the repo type. If
            this argument is not given, the haystack will be fetched every time this function
            is called (only used when working on gitlabinternal). This input is not used when
            a `class_hint` is already given.

    Returns:
        SAHRepo: New SAHRepo instance, using the inferred subclass.
        PlainRepo: New PlainRepo instance when the type inference cannot deduce the exact repository
        type.
    """
    gitlab = Gitlab()
    project = gitlab.gitlab.projects.get(repo_id)
    return repo_create({"id": repo_id, "name": project.name,
                        "namespace": project.namespace["full_path"]}, class_hint, haystack)


def repo_create_by_name(name: Name, class_hint=None, haystack: Haystack = None) -> SAHRepo:
    """ Factory method for creating SAHRepo instances when only a SAHLab Name object is available.

    This method will fetch the remaining information directly from gitlab and forward it to the
    `sahlab.repository.repo_create` function. The rest of the type inference logic is identical.
    Similarly, you can also provide a class_hint to be forwarded as well.

    There is no possibility to encode ownership information of a repository this way.

    Note that this way of creating repositories is significantly slower than providing all the
    required information directly to `sahlab.repository.repo_create`, as more API calls are needed.

    This method is very useful when dealing with dependencies that you can name, but don't know the
    identifiers of (and aren't part of the Haystack).

    Args:
        name (Name): The name of the component you want to fetch.
        class_hint (class, optional): Class hint to skip type inference in
            `sahlab.repository.repo_create`.
        haystack (Haystack, optional): A sahlab haystack object used to find the repo type. If
            this argument is not given, the haystack will be fetched every time this function
            is called (only used when working on gitlabinternal). This input is not used when
            a `class_hint` is already given.

    Returns:
        SAHRepo: New SAHRepo instance, using the inferred subclass.
        PlainRepo: New PlainRepo instance when the type inference cannot deduce the exact repository
        type.
    """
    gitlab = Gitlab()
    project = gitlab.gitlab.projects.get(name.path)
    return repo_create({"id": project.id, "name": project.name,
                        "namespace": project.namespace["full_path"]}, class_hint, haystack)


def repo_create_by_baf_name(name: str, class_hint: SAHRepo = None,
                            haystack: Haystack = None) -> Optional[SAHRepo]:
    """ Deprecated """
    return repo_create_by_plain_name(name, class_hint, haystack)


def repo_create_by_plain_name(name: str,
                              class_hint: SAHRepo = None,
                              haystack: Haystack = None,
                              include_sah_name: bool = True,
                              include_yocto_name: bool = True,
                              include_openwrt_name: bool = True,
                              include_package_name: bool = False) -> Optional[SAHRepo]:
    """ Returns a SAHRepo object for the specified component.
    Returns None when no repository could be found for the component.

    repo = repo_create_by_plain_name("amxrt")

    Args:
        name (str): baf_name, sah_name, yocto_name, package_name or openwrt_name of the component
        class_hint (class, optional): Providing the class hint skips the type inference and
            instead just creates an instance of the given class. It it the responsibility of the
            user to make sure that the class is correct. Minimal checking occurs to make sure the
            class is at least part of the SAHRepo hierachy, which is a requirement.
            Defaults to None.
        haystack (Haystack, optional): A sahlab haystack object used to find the repo type. If
            this argument is not given, the haystack will be fetched every time this function
            is called (only used when working on gitlabinternal). This input is not used when
            a `class_hint` is already given.
        include_sah_name (bool, optional): Include sah names for the search
        include_yocto_name (bool, optional): Include yocto names for the search
        include_openwrt_name (bool, optional): Include openwrt names for the search
        include_package_name (bool, optional): Include package names for the search


    Returns:
        SAHRepo (optional): The SAHRepo object matching the name or None if the name wasn't
            found in any seeded mapping. Be sure to provide all required NameFactory mappings
            before calling this function.
    """
    names = NameFactory().get_name_for_component(name,
                                                 include_sah_name=include_sah_name,
                                                 include_yocto_name=include_yocto_name,
                                                 include_openwrt_name=include_openwrt_name,
                                                 include_package_name=include_package_name)
    if not names:
        return None

    try:
        repo = repo_create_by_name(names[0], class_hint=class_hint, haystack=haystack)
    except GitlabGetError:
        # Something is wrong with the name mapping of this component
        # Probably the path does not match a real git repo
        return None
    return repo


def new_project_create(parent_group_id: int, name: str):
    """ Will create a new project on Gitlab.

    Args:
        parent_group_id (int) : id of the parent group
        name (str): The name of the new project

    Example:
        new_project_create(parent_group_id=1835, name="libamxdummy")
    """
    gitlab = Gitlab()
    new_repo = gitlab.gitlab.projects.create({"name": name,
                                              "namespace_id": parent_group_id})
    return repo_create_by_id(new_repo.id)


def _get_type_from_haystack(repo_def: dict, haystack: Haystack) -> Optional[Type[SAHRepo]]:
    """ Try to find the repo in the haystack and return the correct repo type in the case
    the repo is found. Returns None if the repo cannot be found in the haystack.
    """
    mappings = {"config": ConfigRepo,
                "meta": MetaRepo,
                "yocto": YoctoLayerRepo,
                "yocto-distro": YoctoDistroRepo,
                "openwrt-config": WrtConfigRepo,
                "prpl-config": PrplConfigRepo,
                "openwrt-feed": OpenwrtFeedRepo,
                "container": ContainerRepo,
                "gitlab-ci": GitlabCIRepo,
                "python": PythonRepo,
                }

    types = tuple(filter(None,
                         map(lambda h: mappings.get(h, None),
                             haystack.apply_filter(". | to_entries[] | "
                                                   f'select(.value[] | .id == {repo_def.get("id")})'
                                                   ".key"))))
    if types:
        if len(types) > 1:
            repo_def["types"] = types
            return CompositeRepo
        return types[0]
    return None


def _get_type_from_name(repo_def: dict) -> Optional[Type[SAHRepo]]:
    """ Try to find the correct repo type based on the name or namespace of the repo.
    Returns None if no match can be found.
    """
    mappings = [("-feed", "name", OpenwrtFeedRepo),
                ("feed_", "name", OpenwrtFeedRepo)
                ]

    for item in mappings:
        if item[0] in repo_def.get(item[1], []):
            return item[2]
    return None


def repo_create(repo_def: dict, class_hint=None, haystack: Haystack = None) -> SAHRepo:
    """ Factory method for SAHRepo instances.

    This method provides a general construction mechanism for repositories of all different shapes.
    It takes a description of the repository, as found in the `sahlab.haystack.Haystack`, and an
    optional class hint to infer the correct typing based on its input.

    Type inference occurs by first checking the haystack (when working on gitlabinternal) and then
    parsing the 'name' of the project definition.
    Names with 'meta_' in them are assumed to be "meta" repositories, while projects with "config/"
    in the namespace are assumed to be "config" repositories and so on.
    More inference techniques can be added in the future to be more robust (e.g. using labels).

    Args:
        repo_def (dict): The definition of the project, should at least include an 'id' field
            and a 'name' field.
        class_hint (class, optional): Providing the class hint skips the type inference and
            instead just creates an instance of the given class. It it the responsibility of the
            user to make sure that the class is correct. Minimal checking occurs to make sure the
            class is at least part of the SAHRepo hierachy, which is a requirement.
            Defaults to None.
        haystack (Haystack, optional): A sahlab haystack object used to find the repo type. If
            this argument is not given, the haystack will be fetched every time this function
            is called (only used when working on gitlabinternal). This input is not used when
            a `class_hint` is already given.

    Returns:
        SAHRepo: New SAHRepo instance, using the inferred subclass.
        PlainRepo: New PlainRepo instance when the type inference cannot deduce the exact repository
        type.
    """
    if class_hint and issubclass(class_hint, SAHRepo):
        return class_hint(repo_def)
    if "types" in repo_def and CompositeRepo.validate_types(repo_def["types"]):
        return CompositeRepo(repo_def)

    return_type = None

    current_instance = Gitlab().instance
    if current_instance and "gitlabinternal.softathome.com" in current_instance and not haystack:
        haystack = Haystack()

    if haystack:
        return_type = _get_type_from_haystack(repo_def, haystack)

    # Try to infer type from name
    if not return_type:
        return_type = _get_type_from_name(repo_def)

    # Try to infer type from project labels
    # TODO

    # Return
    return return_type(repo_def) if return_type else PlainRepo(repo_def)


class Dependency():
    """ Dependency object keeps information on itself (its name and version) as well as where it
    comes from (its dependant). Dependency objects can be pickled (python slang for serializationi)
    and can thus be processed in parallel processes. """

    def __init__(self,
                 name: str,
                 ref: str,
                 found_in: str,
                 dependant: SAHRepo,
                 dependant_ref: str,
                 dependant_sha: str):
        self.name = name
        self.ref = ref
        self.found_in = found_in
        self.dependant = dependant
        self.dependant_ref = dependant_ref
        self.dependant_sha = dependant_sha

    def __str__(self):
        return f"{self.name}:{self.ref} (as found in {str(self.dependant)}:"\
               f"{str(self.dependant_ref)} [{self.found_in}])"

    def __getstate__(self):
        state = self.__dict__.copy()
        # Change dependant object into pickleable tuple
        state["dependant"] = (self.dependant.definition, self.dependant.__class__)
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.dependant = repo_create(state["dependant"][0], state["dependant"][1])

    def update(self, version: str, opensource_version: str = "",
               integration_branch: str = "") -> str:
        """ Return the contents of the dependency file (self.found_in) with this dependency updated
        to the given new version.
        If the integration branch is given, then the update will be reflected using the contents of
        the given branch.

        Args:
            version (str): The version to upgrade to.
            opensource_version (str, optional): The opensource version
            integration_branch (str, optional): Update the dependency to the given version based on
                the dependency information stored on the integration branch. If unset, then the
                reference will be used where the dependency was found on (dependant_sha).

        Returns:
            str: The string representation of the updated dependency file. This string can then be
                 used to create a new commit to actually update the file.
        """
        return self.dependant.update_dependency(self, version,
                                                opensource_version, integration_branch)

    def is_whitelisted(self, branch: str = "", repo: int = 6747) -> bool:
        """ Returns whether or not this dependency is present on the "whitelist".
        This whitelist is specifically for updating `sahlab.repository.ConfigRepo` instances.
        The default used whitelist can be found here:
        https://gitlab.com/soft.at.home/sofa/integration-whitelist/-/blob/main/whitelist.

        Args:
            branch (str, optional): The version of the whitelist to use.
                If not specified it will default to
                the environment variable WHITELIST_BRANCH, if that variable is not specified, it
                will default to "master".
            repo (int, optional): The repository to look for the whitelist file, default to 6747.

        Returns:
            bool: If the dependency name is matched in the whitelist file.
        """
        if not branch:
            branch = os.environ.get("WHITELIST_BRANCH", "master")
        project = repo_create_by_id(repo, PlainRepo)
        whitelist = project.get_raw_file("whitelist", ref=branch)
        return any(bool(re.match(w, self.name)) for w in filter(None, whitelist.split("\n")))

    # def get_project(self):
    #     """Returns the SAHRepo instance of this Dependency. """
    #     raise NotImplementedError("Dependency to project resolution requires "
    #                               "implementation of Component.lst translation")
