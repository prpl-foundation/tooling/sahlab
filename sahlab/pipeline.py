""" Pipeline interaction API """
import asyncio
import functools
import time
from datetime import datetime as dt
from enum import Enum
from typing import Any, List


class CiResult(Enum):
    """ Different possible states for a pipeline/job to be in. This is a reduced set of possible
    states compared to the actual set of states. This is to simplify calculations with states.
    This shouldn't impact most use-cases.
    """
    RUNNING = -1
    FAILED = 0
    SUCCESS = 1
    WARNING = 2
    CANCELED = 10
    SKIPPED = 20

    def __int__(self):
        return self.value

    def __str__(self):
        return str(self.name).lower()

    @staticmethod
    def from_status(status: str) -> 'CiResult':
        """ Converts the gitlab status string to a CiResult enum object. """
        if status in ("created", "waiting_for_resource", "preparing", "pending", "manual"):
            status = "running"
        return CiResult[status.upper()]

    @staticmethod
    def from_job(job) -> 'CiResult':
        """ Converts a job to a CiResult (taking into account allow failure state). """
        status = job.status
        if job.status == 'failed' and job.allow_failure:
            status = 'warning'
        return CiResult.from_status(status)

    @staticmethod
    def aggregate(results: List['CiResult']) -> 'CiResult':
        """ Group together a list of CI results to a single CI result. If the list is empty,
        CiResult.SKIPPED will be returned. """
        if CiResult.CANCELED in results:
            return CiResult.CANCELED
        results = list(filter(CiResult.SKIPPED.__ne__, results))
        if not results:
            return CiResult.SKIPPED
        combination = functools.reduce(lambda a, b: (-2 * (a < 0 or b < 0) + 1) * abs(a) * abs(b),
                                       map(int, results))
        return CiResult(min(2, max(-1, int(combination))))


class Pipeline:
    """ The Pipeline object provides all functionality to interact with an existing pipeline.
    To create new Pipeline objects (either by starting a new pipeline or retrieving an existing
    pipeline), refer to the pipeline related methods in the `sahlab.repository.SAHRepo` class.
    """
    def __init__(self, pipeline):
        self.pipeline = pipeline

    def get_attribute(self, attribute: str) -> Any:
        """ Returns an attribute of the embedded gitlab pipeline.
        """
        return self.pipeline.attributes[attribute]

    async def wait_for_pipeline(self,
                                exclude_jobs: List[str] = None,
                                timeout: int = 600,
                                step_time: int = 10) -> CiResult:
        """ Wait async for the pipeline to finish. Excluded jobs are not taken into account.
        That allows this method to be used inside a pipeline. Returns the pipeline state
        (potentially filtered with excluded jobs).
        """
        time_max = time.time() + timeout
        while time.time() < time_max:
            if not exclude_jobs:
                status = self.get_status()
            else:
                if not self.pipeline.started_at \
                   or dt.timestamp(dt.fromisoformat(self.pipeline.started_at)) - time.time() < 10 \
                   and not self.pipeline.jobs.list():
                    # Pipeline has just been created and could have no jobs registered
                    await asyncio.sleep(step_time)
                jobs = [j for j in self.pipeline.jobs.list(all=True, per_page=100)
                        if j.name not in exclude_jobs]
                status = CiResult.aggregate(list(map(CiResult.from_job, jobs)))
            if status != CiResult.RUNNING:
                return status
            await asyncio.sleep(step_time)
        raise TimeoutError(f"Timeout exceeded while waiting for pipeline {self.pipeline.id}")

    async def wait_for_jobs(self,
                            job_names: List[str],
                            timeout: int = 600,
                            step_time: int = 10) -> CiResult:
        """ Wait async for a selection of jobs. Returns the aggregated result state of the given
        jobs. """
        time_max = time.time() + timeout
        while time.time() < time_max:
            if not self.pipeline.started_at \
               or dt.timestamp(dt.fromisoformat(self.pipeline.started_at)) - time.time() < 10 \
               and not self.pipeline.jobs.list():
                # Pipeline has just been created and could have no jobs registered
                await asyncio.sleep(step_time)
            jobs = [j for j in self.pipeline.jobs.list(all=True, per_page=100)
                    if j.name in job_names]
            status = CiResult.aggregate(list(map(CiResult.from_job, jobs)))
            if status != CiResult.RUNNING:
                return status
            await asyncio.sleep(step_time)
        raise TimeoutError(f"Timeout exceeded while waiting for job in pipeline {self.pipeline.id}")

    def get_status(self) -> CiResult:
        """ Returns the CiResult status of the pipeline """
        self.pipeline.refresh()
        return CiResult.from_status(self.pipeline.status)
