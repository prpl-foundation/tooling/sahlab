""" Gitlab Group object and management """
from typing import Any, Iterable, Union

from sahlab.gitlab import Gitlab
from sahlab.repository import repo_create_by_id, SAHRepo


class Group():
    """ Default Gitlab Group object. The init function takes as input either an int
    (the group identifier) or a string (the path to the group).
    """
    def __init__(self, group: Union[int, str]):
        gitlab = Gitlab()
        self.group = gitlab.gitlab.groups.get(group)
        self.wrapped = self.group

    def __str__(self):
        return f'{self.__class__.__name__}(id={self.get_attribute("id")}, '\
               f'path={self.get_attribute("full_path")})'

    def get_attribute(self, attribute: str) -> Any:
        """ Returns an attribute of the embedded gitlab group. """
        return self.group.attributes[attribute]

    def update_attributes(self, **kwargs: Any):
        """ Updates the given attributes in the kwargs dictionary.
        A given attribute must of course exist.
        """
        list(map(lambda e: setattr(self.group, e, kwargs[e]), kwargs))
        self.group.save()

    def get_projects(self, with_shared: bool = False,
                     include_subgroups: bool = False, archived: bool = None) -> Iterable[SAHRepo]:
        """ Returns SAHRepo objects for each project in the group. Note that this can take a long
        time for especially large groups (e.g. network group).

        Args:
            with_shared (bool, optional): Include projects shared to this group. Default is set to
            False.
            include_subgroups (bool, optional): True to include all the projects in subgroups.
            Default set to False.
            archived (bool, optional): Set archived to True to only retrieve archived projects,
            False to exclude archived projects or leave blank to retrieve both archived and
            non-archived
        """
        return map(lambda p: repo_create_by_id(p.id),
                   self.group.projects.list(with_shared=with_shared,
                                            include_subgroups=include_subgroups,
                                            archived=archived,
                                            iterator=True))

    def get_subgroups(self) -> Iterable['Group']:
        """ Returns Group objects for each sub-group in the group. """
        return map(lambda g: Group(g.id), self.group.subgroups.list(iterator=True))


def new_group_create(parent_group_id: int, name: str):
    """ Will create a new Subgroup on Gitlab. Top-level groups are currently not permitted.

    Args:
        parent_group_id (int): The id of the parent group
        name (str): The name of the new project

    Example:
        new_project_create(parent_group_id=1935, name="libamxdummy")
    """
    gitlab = Gitlab()
    new_group = gitlab.gitlab.groups.create({'name': name,
                                             'path': name,
                                             'parent_id': parent_group_id})
    new_group_id = new_group.get_id()
    assert new_group_id is not None
    return Group(int(new_group_id))
