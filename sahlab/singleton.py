""" Singleton class for unique objects """
import inspect
from typing import Dict, Optional


class Singleton(type):
    """ Metaclass for creating Singleton objects """
    _instances: Dict[type, 'Singleton'] = {}
    _keys: Dict[type, str] = {}
    _dyn_default: Dict[type, Optional[str]] = {}

    def __new__(cls, name, bases, namespace, **_kwargs):
        return super().__new__(cls, name, bases, namespace)

    def __init__(cls, name, bases, namespace, **kwargs):
        if cls not in cls._keys:
            cls._keys[cls] = kwargs.get('key', cls)
            cls._dyn_default[cls] = kwargs.get('dyn_default', None)
        super().__init__(name, bases, namespace)

    def __call__(cls, *args, **kwargs):
        if cls._keys[cls] is not cls:
            if cls._dyn_default[cls]:
                default = cls._dyn_default[cls]()
            else:
                default = inspect.signature(cls.__init__).parameters.get(cls._keys[cls]).default
            if (cls, kwargs.get(cls._keys[cls], default)) not in cls._instances:
                cls._instances[(cls, kwargs.get(cls._keys[cls], default))] \
                        = super(Singleton, cls).__call__(*args, **kwargs)
            return cls._instances[(cls, kwargs.get(cls._keys[cls], default))]
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
